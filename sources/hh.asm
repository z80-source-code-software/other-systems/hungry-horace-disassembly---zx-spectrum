; Hungry Horace disassembly
; https://skoolkit.ca
;
; Copyright 1982 Beam Software (Hungry Horace)
; Copyright 2014-2019 Richard Dymond (this disassembly)

  ORG 24576

; The game has just loaded
START:
  DI                      ; Disable interrupts.
  LD A,(23672)            ; Initialise the pseudo-random number address.
  LD L,A                  ;
  AND 31                  ;
  LD H,A                  ;
  LD A,(HL)               ;
  LD L,A                  ;
  AND 31                  ;
  LD H,A                  ;
  LD (RANDADDR),HL        ;
  LD HL,TPRESS            ; Initialise the 'T' pressed indicator to 'off' (bit
  LD (HL),16              ; 4 set).
  LD HL,SOUND             ; Initialise the sound indicator to 'on'.
  LD (HL),31              ;
; This entry point is used by the routine at DEAD when a game has ended.
START_0:
  LD HL,22528             ; INK 7: PAPER 7
  LD DE,22529             ;
  LD BC,767               ;
  LD (HL),63              ;
  LDIR                    ;
  LD HL,16384             ; Clear the display file.
  LD DE,16385             ;
  LD BC,6143              ;
  LD (HL),0               ;
  LDIR                    ;
  LD HL,22528             ; INK 0: PAPER 7
  LD DE,22529             ;
  LD BC,767               ;
  LD (HL),56              ;
  LDIR                    ;
  LD A,0                  ; Signal demo mode.
  LD (GAMEMODE),A         ;
  LD HL,140               ; Initialise the title screen countdown timer.
  LD (TITLECT),HL         ;
  LD DE,16544             ; Print 'HUNGRY' in big letters.
  LD HL,HUNGRY            ;
  LD BC,160               ;
  CALL BIGWORD            ;
  LD DE,18560             ; Print 'HORACE' in big letters.
  LD HL,HORACE            ;
  LD BC,160               ;
  CALL BIGWORD            ;
  LD A,(GAMEMODE)         ; Pick up the game mode indicator.
  PUSH AF                 ; Save it briefly.
  LD A,1                  ; Signal game mode (to force the copyright message to
  LD (GAMEMODE),A         ; be printed).
  LD HL,20672             ; This is the display file address for the copyright
                          ; message.
  CALL ATTRADDR           ; Set HL' to the corresponding attribute file address
                          ; (unnecessarily).
  EX DE,HL                ; Transfer the display file address to DE.
  LD HL,COPYRIGHT         ; Print 'Copyright © 1982 Beam Software' and 'PRESS
START_1:
  LD A,(HL)               ; ANY KEY TO START PLAY'.
  CP 255                  ;
  JR Z,START_2            ;
  INC HL                  ;
  CALL PRINTDIGIT_0       ;
  JR START_1              ;
START_2:
  POP AF                  ; Restore the game mode indicator.
  LD (GAMEMODE),A         ;
START_3:
  LD HL,22688             ; Change the colour of the words 'HUNGRY HORACE' on
  LD DE,22689             ; the title screen.
  LD BC,159               ;
  LD A,(HL)               ;
  INC A                   ;
  AND 63                  ;
  OR 56                   ;
  LD (HL),A               ;
  LDIR                    ;
  LD HL,22912             ;
  LD DE,22913             ;
  LD BC,159               ;
  LD A,(HL)               ;
  INC A                   ;
  AND 63                  ;
  OR 56                   ;
  LD (HL),A               ;
  LDIR                    ;
  CALL TITLESOUND         ; Make the title screen sound effect.
  LD HL,(TITLECT)         ; Decrement the title screen countdown timer.
  DEC HL                  ;
  LD (TITLECT),HL         ;
  LD A,H                  ; Is it zero now?
  OR L                    ;
  JR Z,START_4            ; Jump if so.
  LD A,0                  ; Read the keyboard.
  IN A,(254)              ;
  AND 31                  ; Is a key being pressed?
  CP 31                   ;
  JR Z,START_3            ; Jump if not.
  LD A,1                  ; Signal that a game (not demo mode) has started.
  LD (GAMEMODE),A         ;
  JR START_4              ; This instruction is redundant.
START_4:
  LD A,255                ; Initialise the maze number.
  LD (MAZENO),A           ;
  LD A,8                  ; Initialise the game speed parameter.
  LD (SPEED),A            ;
  LD A,3                  ; Initialise the number of lives remaining.
  LD (LIVES),A            ;
  LD HL,0                 ; Initialise the score.
  LD (SCORE),HL           ;
  LD A,0                  ; Initialise the active guard counter.
  LD (GUARDCOUNT),A       ;
  LD (ELIFEIND),A         ; Initialise the extra life indicator.
  CALL INITGCT            ; Initialise the guard countdown timers.
; This entry point is used when moving from one maze to the next by the
; routines at DEAD (in demo mode) and NEXTMAZE (in game mode).
START_5:
  LD A,(MAZENO)           ; Pick up the current maze number (0-3).
  INC A                   ; Is it 0, 1 or 2?
  AND 252                 ;
  JR Z,START_7            ; Jump if so.
  LD A,(SPEED)            ; Decrement the game speed parameter (unless it's
  DEC A                   ; already 1). This increases the speed of the game.
  JR NZ,START_6           ;
  LD A,1                  ;
START_6:
  LD (SPEED),A            ;
START_7:
  LD A,(MAZENO)           ; Increment the maze number.
  INC A                   ;
  AND 3                   ;
  LD (MAZENO),A           ;
  LD A,(MAZENO)           ; Pick up the current maze number.
  CP 0                    ; Is it maze 1?
  JR NZ,START_8           ; Jump if not.
  LD HL,MAZE1DATA         ; Copy the tunnel offset and bell, initial guard and
  CALL CPMAZEDATA         ; entrance locations for maze 1 to CMTUNNEL.
  LD HL,MAZE1             ; Point HL at the layout data for maze 1.
  JR START_11
START_8:
  CP 1                    ; Is it maze 2?
  JR NZ,START_9           ; Jump if not.
  LD HL,MAZE2DATA         ; Copy the tunnel offset and bell, initial guard and
  CALL CPMAZEDATA         ; entrance locations for maze 2 to CMTUNNEL.
  LD HL,MAZE2             ; Point HL at the layout data for maze 2.
  JR START_11
START_9:
  CP 2                    ; Is it maze 3?
  JR NZ,START_10          ; Jump if not.
  LD HL,MAZE3DATA         ; Copy the tunnel offset and bell, initial guard and
  CALL CPMAZEDATA         ; entrance locations for maze 3 to CMTUNNEL.
  LD HL,MAZE3             ; Point HL at the layout data for maze 3.
  JR START_11
START_10:
  LD HL,MAZE4DATA         ; Copy the tunnel offset and bell, initial guard and
  CALL CPMAZEDATA         ; entrance locations for maze 4 to CMTUNNEL.
  LD HL,MAZE4             ; Point HL at the layout data for maze 4.
  JR START_11
START_11:
  LD DE,16384             ; Draw the current maze.
  LD BC,768               ;
  CALL DRAWMAZE           ;
  LD A,(GAMEMODE)         ; Pick up the game mode indicator.
  AND A                   ; Is it demo mode?
  JR Z,START_13           ; Jump if so.
  LD HL,16384             ; This is the display file address for 'PASSES
                          ; SCORE       BEST'.
  CALL ATTRADDR           ; Set HL' to the corresponding attribute file address
                          ; (unnecessarily).
  EX DE,HL                ; Transfer the display file address to DE.
  LD HL,STATUS            ; Print 'PASSES   SCORE       BEST'.
START_12:
  LD A,(HL)               ;
  CP 255                  ;
  JR Z,START_17           ;
  INC HL                  ;
  CALL PRINTDIGIT_0       ;
  JR START_12             ;
START_13:
  LD A,1                  ; Signal game mode (to force the 'DEMO MODE' text to
  LD (GAMEMODE),A         ; be printed).
  LD HL,16384             ; This is the display file address for 'DEMO MODE
                          ; PRESS ANY KEY TO PLAY'.
  CALL ATTRADDR           ; Set HL' to the corresponding attribute file address
                          ; (unnecessarily).
  EX DE,HL                ; Transfer the display file address to DE.
  LD HL,DEMOMODE          ; Print 'DEMO MODE  PRESS ANY KEY TO PLAY'.
START_14:
  LD A,(HL)               ;
  CP 255                  ;
  JR Z,START_15           ;
  INC HL                  ;
  CALL PRINTDIGIT_0       ;
  JR START_14             ;
START_15:
  LD A,0                  ; Restore the game mode indicator to demo mode.
  LD (GAMEMODE),A         ;
  LD HL,22528             ; Make 'DEMO MODE' flash.
  LD B,9                  ;
START_16:
  LD A,(HL)               ;
  ADD A,128               ;
  LD (HL),A               ;
  INC HL                  ;
  DJNZ START_16           ;
START_17:
  LD HL,(CMBELLLOC)       ; Copy the bell location to BELLLOC.
  LD (BELLLOC),HL         ;
  LD HL,16391             ; This is the display file address for the number of
                          ; lives remaining.
  CALL ATTRADDR           ; Set HL' to the corresponding attribute file address
                          ; (unnecessarily).
  LD A,(LIVES)            ; Pick up the number of lives remaining.
  ADD A,"0"               ; Convert it to the ASCII code for the corresponding
                          ; digit.
  EX DE,HL                ; Transfer the display file address to DE.
  CALL PRINTDIGIT_0       ; Print the number of lives remaining.
  LD HL,2000              ; Initialise the lunch-drop countdown timer.
  LD (LUNCHCT),HL         ;
; This entry point is used by the routine at DEAD after Horace has lost a life.
START_18:
  LD HL,(BELLLOC)         ; Pick up the bell location.
  LD A,H                  ; Has Horace sounded the alarm?
  OR L                    ;
  JR Z,START_19           ; Jump if so.
  LD HL,(CMBELLLOC)       ; Reinitialise the bell location.
  LD (BELLLOC),HL         ;
START_19:
  LD B,4                  ; There are four guards.
START_20:
  PUSH BC                 ; Copy the guard's buffer into the temporary location
  CALL COPYGUARD          ; (GUARDLOC).
  POP BC                  ;
  PUSH BC                 ; Save the guard counter.
  SLA B                   ; Point HL at one of the guard countdown timers at
  LD C,B                  ; GUARD1CT.
  LD B,0                  ;
  XOR A                   ;
  LD HL,28078             ;
  SBC HL,BC               ;
  LD A,(HL)               ; Is this guard in play?
  INC HL                  ;
  OR (HL)                 ;
  JR NZ,START_21          ; Jump if not.
  LD HL,(CMINITGL)        ; Pick up the initial guard location for the current
                          ; maze.
  JR START_22
START_21:
  LD HL,(CMENTRANCE)      ; Pick up the bell location for the current maze.
START_22:
  LD (GUARDLOC),HL        ; Initialise the guard's current and new locations.
  LD (GUARDNLOC),HL       ;
  LD HL,GUARDBG           ; Clear the four maze background tiles in the
  LD C,4                  ; temporary guard buffer.
START_23:
  LD (HL),62              ;
  INC HL                  ;
  LD B,8                  ;
START_24:
  LD (HL),0               ;
  INC HL                  ;
  DJNZ START_24           ;
  DEC C                   ;
  JR NZ,START_23          ;
  CALL UPDTGUARD          ; Copy the guard's buffer back to the original
                          ; location.
  POP BC                  ; Restore the guard counter to B.
  DJNZ START_20           ; Jump back to prepare the next guard.
  LD HL,(CMENTRANCE)      ; Pick up Horace's initial location for the current
                          ; maze.
  LD (HORACELOC),HL       ; Initialise Horace's current and new locations.
  LD (HORACENLOC),HL      ;
  LD A,1                  ; Initialise Horace's animation frame.
  LD (HORACEAF),A         ;
  LD A,(SPEED)            ; Initialise the sprite movement timer (using the
  LD (MVTIMER),A          ; value of the game speed parameter).
  LD HL,(SCORE)           ; Print the score and make a sound effect.
  CALL CHKEATEN_1         ;
  LD HL,(HISCORE)         ; Pick up the high score.
  LD DE,16384             ; Set DE to the display file address for the high
  EX DE,HL                ; score (in a roundabout way).
  LD BC,26                ;
  ADD HL,BC               ;
  EX DE,HL                ;
  CALL PRINTSCORE         ; Print the high score.
  LD HL,0                 ; Initialise the guard panic timer.
  LD (GUARDPT),HL         ;
  LD A,57                 ; Initialise Horace's attribute byte (INK 1: PAPER
  LD (HORACEATTR),A       ; 7).
; This routine continues into the main loop at MAINLOOP.

; Main loop
;
; The routine at START continues here.
MAINLOOP:
  CALL READKEYS           ; Read the keyboard and change Horace's sprite
                          ; accordingly.
  CALL MVSPRITES          ; Move Horace and the guards.
  CALL REDRAW             ; Redraw the tiles behind Horace and the guards if
                          ; they've moved.
  CALL CHKEATEN           ; Add to the score if Horace has eaten something.
  CALL DRAWGUARDS         ; Draw the guards.
  CALL CHKHIT             ; Check whether Horace has run into a guard.
  CALL DRAWHORACE         ; Draw Horace.
  CALL DRAWBELL           ; Draw the bell and check whether Horace has sounded
                          ; the alarm.
  CALL CHKLUNCH           ; Check whether a guard should drop his lunch.
  CALL DECGTIMERS         ; Decrement the guard countdown timers.
  CALL TOGGLESND          ; Toggle the sound on/off if 'T' is pressed.
  CALL RINGBELL           ; Ring the bell if necessary.
  JR MAINLOOP             ; Jump back to the start of the main loop.

; Read the keyboard and update Horace's sprite accordingly
;
; Called from the main loop at MAINLOOP.
READKEYS:
  LD A,(TUNNELTIME)       ; Pick up the tunnel timer.
  AND A                   ; Is Horace in a tunnel at the moment?
  RET NZ                  ; Return if so.
  LD A,255                ; Initialise the direction keypress to 'no key'.
  LD (DIRKEY),A           ;
  LD A,(GAMEMODE)         ; Pick up the game mode indicator.
  AND A                   ; Is it demo mode?
  JR NZ,READKEYS_6        ; Jump if not.
  LD A,0                  ; Read the keyboard.
  IN A,(254)              ;
  AND 31                  ; Is a key being pressed?
  CP 31                   ;
  JR Z,READKEYS_0         ; Jump if not.
  POP HL                  ; Drop the return address from the stack.
  JP DEAD_8               ; Cycle the screen colours briefly and return to the
                          ; title screen.
; It's demo mode. Time to figure out where to move Horace next.
READKEYS_0:
  LD B,4                  ; Initialise B (the direction indicator) for the loop
                          ; that follows.
READKEYS_1:
  DEC B                   ; B=3 (left), 2 (down), 1 (right) or 0 (up).
  LD HL,(HORACELOC)       ; Pick up Horace's current location.
  LD C,0                  ; Initialise C to 0; this is the direction
                          ; probability parameter.
  PUSH BC                 ; Save the direction indicator briefly.
  LD A,B                  ; Copy the direction indicator to A.
  CALL CHKTILES           ; Check the tiles next to Horace in that direction.
  POP BC                  ; Restore the direction indicator to B.
  CP 2                    ; Is there a wall or the maze exit or entrance in
                          ; that direction?
  JP NC,READKEYS_3        ; Jump if so with C=0: that direction is blocked.
  CALL RANDOM             ; Set C to a pseudo-random number between 25 and 56;
  AND 31                  ; the higher this value, the more likely Horace will
  ADD A,25                ; turn 90 degrees in the direction indicated by B
  LD C,A                  ; when his path is blocked.
  LD A,(HORACEAF)         ; Pick up Horace's animation frame.
  CP B                    ; Is it equal to the current value of the direction
                          ; indicator?
  JR NZ,READKEYS_2        ; Jump if not.
  LD A,40                 ; Now C holds a pseudo-random number between 65 and
  ADD A,C                 ; 96; this value ensures that Horace will keep moving
  LD C,A                  ; in the same direction if he can.
  JR READKEYS_3
READKEYS_2:
  ADD A,2                 ; Add 2 to Horace's animation frame; this has the
  AND 3                   ; effect of toggling his direction between up/down
                          ; and left/right.
  CP B                    ; Is it equal to the current value of the direction
                          ; indicator now?
  JR NZ,READKEYS_3        ; Jump if not.
  LD A,246                ; Now C holds a pseudo-random number between 15 and
  ADD A,C                 ; 46; the higher this value, the more likely Horace
  LD C,A                  ; will turn round 180 degrees when his path is
                          ; blocked.
READKEYS_3:
  PUSH BC                 ; Save the direction indicator briefly.
  LD HL,TEMPDPP           ; Point HL at one of the four slots at TEMPDPP.
  LD C,B                  ;
  LD B,0                  ;
  ADD HL,BC               ;
  POP BC                  ; Restore the direction indicator to B.
  LD A,C                  ; Save the direction probability parameter in the
  LD (HL),A               ; appropriate slot.
  INC B                   ; Have we considered every direction yet?
  DEC B                   ;
  JR NZ,READKEYS_1        ; Jump back if not.
; Having computed a direction probability parameter for each of the four slots
; at TEMPDPP, we now use those parameters to determine Horace's next animation
; frame (and therefore direction of travel).
  XOR A                   ; Compute in C the index of the slot that holds the
  LD HL,28086             ; largest number (0, 1, 2 or 3).
  LD B,4                  ;
READKEYS_4:
  DEC B                   ;
  DEC HL                  ;
  CP (HL)                 ;
  JR NC,READKEYS_5        ;
  LD A,(HL)               ;
  LD C,B                  ;
READKEYS_5:
  INC B                   ;
  DEC B                   ;
  JR NZ,READKEYS_4        ;
  LD A,C                  ; Update Horace's animation frame to this index
  LD (HORACEAF),A         ; value.
  RET
; A game is in progress.
READKEYS_6:
  LD A,251                ; Read keys Q-W-E-R-T.
  IN A,(254)              ;
  AND 1                   ; Is 'Q' (up) being pressed?
  JR Z,READKEYS_7         ; Jump if so.
  LD A,247                ; Read keys 1-2-3-4-5.
  IN A,(254)              ;
  AND 8                   ; Is '4' (up) being pressed?
  JR NZ,READKEYS_8        ; Jump if not.
READKEYS_7:
  LD A,0                  ; Signal: 'up' key pressed.
  LD (DIRKEY),A           ;
  JR READKEYS_14
READKEYS_8:
  LD A,254                ; Read keys SHIFT-Z-X-C-V.
  IN A,(254)              ;
  AND 2                   ; Is 'Z' (down) being pressed?
  JR Z,READKEYS_9         ; Jump if so.
  LD A,247                ; Read keys 1-2-3-4-5.
  IN A,(254)              ; Is '3' (down) being pressed?
  AND 4                   ; Jump if not.
  JR NZ,READKEYS_10
READKEYS_9:
  LD A,2                  ; Signal: 'down' key pressed.
  LD (DIRKEY),A           ;
  JR READKEYS_14
READKEYS_10:
  LD A,223                ; Read keys Y-U-I-O-P.
  IN A,(254)              ;
  BIT 0,A                 ; Is 'P' (right) being pressed?
  JR Z,READKEYS_11        ; Jump if so.
  LD A,247                ; Read keys 1-2-3-4-5.
  IN A,(254)              ;
  AND 2                   ; Is '2' (right) being pressed?
  JR NZ,READKEYS_12       ; Jump if not.
READKEYS_11:
  LD A,1                  ; Signal: 'right' key pressed.
  LD (DIRKEY),A           ;
  JR READKEYS_14
READKEYS_12:
  LD A,223                ; Read keys Y-U-I-O-P.
  IN A,(254)              ;
  BIT 2,A                 ; Is 'I' (left) being pressed?
  JR Z,READKEYS_13        ; Jump if so.
  LD A,247                ; Read keys 1-2-3-4-5.
  IN A,(254)              ;
  AND 1                   ; Is '1' (left) being pressed?
  JR NZ,READKEYS_15       ; Jump if not.
READKEYS_13:
  LD A,3                  ; Signal: 'left' key pressed.
  LD (DIRKEY),A           ;
READKEYS_14:
  LD HL,(HORACELOC)       ; Pick up Horace's current location.
  CALL CHKTILES           ; Check the tiles in front of Horace.
  CP 3                    ; Is Horace facing a wall?
  JR NZ,READKEYS_15       ; Jump if not.
  LD A,255                ; Signal: no key pressed (Horace will not respond to
  LD (DIRKEY),A           ; the keypress).
READKEYS_15:
  LD A,(DIRKEY)           ; Pick up the direction keypress indicator.
  CP 255                  ; Was a direction key pressed?
  RET Z                   ; Return if not.
  LD (HORACEAF),A         ; Otherwise update Horace's animation frame
                          ; accordingly.
  RET

; Move Horace and the guards
;
; Called from the main loop at MAINLOOP.
MVSPRITES:
  LD A,(MVTIMER)          ; Decrement the sprite movement timer.
  DEC A                   ;
  LD (MVTIMER),A          ;
  RET NZ                  ; Return unless it's zero.
  LD A,(SPEED)            ; Reset the sprite movement timer to the value of the
  LD (MVTIMER),A          ; game speed parameter.
  LD A,(TUNNELTIME)       ; Pick up the tunnel timer.
  AND A                   ; Is Horace in a tunnel at the moment?
  JR NZ,MVSPRITES_10      ; Jump if so.
  LD A,(HORACEAF)         ; Pick up Horace's animation frame.
  LD HL,(HORACELOC)       ; Pick up Horace's current location.
  CALL CHKTILES           ; Check the tiles in front of Horace.
  CP 0                    ; Is there anything in front of Horace?
  JR Z,MVSPRITES_7        ; Jump if not.
  CP 1                    ; Is Horace facing a tunnel entrance?
  JR Z,MVSPRITES_0        ; Jump if so.
  CP 2                    ; Is Horace facing a maze exit or entrance?
  JR NZ,MVSPRITES_9       ; Jump if not.
  LD A,(HORACEAF)         ; Pick up Horace's animation frame.
  CP 1                    ; Is Horace facing right?
  JP Z,NEXTMAZE           ; Jump if so (Horace is leaving the maze).
  JR MVSPRITES_9
; Horace is about to enter a tunnel.
MVSPRITES_0:
  LD A,90                 ; Initialise the tunnel timer.
  LD (TUNNELTIME),A       ;
  LD BC,(CMTUNNEL)        ; Pick up the tunnel offset for the current maze.
  LD HL,(HORACELOC)       ; Pick up Horace's location.
  LD A,(HORACEAF)         ; Pick up Horace's animation frame.
  AND A                   ; Is Horace going up?
  JR Z,MVSPRITES_1        ; Jump if so.
  SBC HL,BC               ; Subtract the tunnel offset from Horace's location.
  JR MVSPRITES_2
MVSPRITES_1:
  ADD HL,BC               ; Add the tunnel offset to Horace's location.
MVSPRITES_2:
  PUSH HL                 ; Save HL (which holds Horace's new location)
                          ; temporarily.
  LD HL,SOUND             ; Pick up the sound on/off indicator.
  LD C,16                 ; Initialise C for the loop that follows.
MVSPRITES_3:
  PUSH BC                 ; Save the loop counter (unnecessarily).
  LD B,C                  ; Set B and E equal to 8*C. This value determines the
  SLA B                   ; pitch.
  SLA B                   ;
  SLA B                   ;
  LD E,B                  ;
  LD D,8                  ; This value determines the duration.
MVSPRITES_4:
  LD B,E                  ; Produce a sound (if the sound on/off indicator is
  LD A,31                 ; on) with pitch and duration determined by E and D.
  AND (HL)                ;
  OUT (254),A             ;
MVSPRITES_5:
  DJNZ MVSPRITES_5        ;
  LD A,7                  ;
  OUT (254),A             ;
  LD B,E                  ;
MVSPRITES_6:
  DJNZ MVSPRITES_6        ;
  DEC D                   ;
  JR NZ,MVSPRITES_4       ;
  POP BC                  ; Restore the loop counter to C (unnecessarily).
  DEC C                   ; Finished yet?
  JR NZ,MVSPRITES_3       ; Jump back if not.
  POP HL                  ; Restore Horace's new location to HL.
  JR MVSPRITES_8
; There's nothing in front of Horace, so he can move one space forward.
MVSPRITES_7:
  LD HL,(HORACELOC)       ; Pick up Horace's current location.
  LD A,(HORACEAF)         ; Pick up Horace's animation frame.
  CALL INFRONT            ; Get the location of the spot one space in front of
                          ; Horace.
MVSPRITES_8:
  LD (HORACENLOC),HL      ; Update Horace's location.
  JR MVSPRITES_10
; Horace is facing a wall or the maze entrance, and so cannot move forward.
MVSPRITES_9:
  LD HL,(HORACELOC)       ; Pick up Horace's current location.
  LD (HORACENLOC),HL      ; Set Horace's new location.
; Now it's time to move the guards.
MVSPRITES_10:
  LD HL,GUARD1CT          ; Point HL at the first of the guard countdown
                          ; timers.
  LD B,4                  ; There are four guards to consider.
MVSPRITES_11:
  PUSH BC                 ; Save the guard counter.
  LD A,(HL)               ; Set the zero flag if this guard is in play.
  INC HL                  ;
  OR (HL)                 ;
  INC HL                  ; Point HL at the next guard's countdown timer.
  PUSH HL                 ; Save the guard countdown timer pointer.
  JR NZ,MVSPRITES_12      ; Jump if this guard is not in play at the moment.
  CALL COPYGUARD          ; Copy the guard's buffer into the temporary location
                          ; (GUARDLOC).
  CALL MVGUARD            ; Move the guard.
  CALL UPDTGUARD          ; Copy the guard's buffer back to the original
                          ; location.
MVSPRITES_12:
  POP HL                  ; Restore the guard countdown timer pointer to HL.
  POP BC                  ; Restore the guard counter to B.
  DJNZ MVSPRITES_11       ; Jump back to handle the next guard.
  RET

; Move a guard
;
; Used by the routine at MVSPRITES.
MVGUARD:
  LD A,(GUARDDELAY)       ; Pick up the guard's return delay counter.
  AND A                   ; Has this guard been thrown out of the park?
  RET NZ                  ; Return if so.
  LD HL,(HORACELOC)       ; Pick up Horace's current location.
  RR H                    ; Now H=0, 1 or 2, indicating which third of the
  RR H                    ; screen the top row of Horace's sprite is in.
  RR H                    ;
  LD B,5                  ; Move bits 5-7 of L down to bits 0-2, and bits 0-2
MVGUARD_0:
  RR H                    ; of H into bits 3-5 of L.
  RR L                    ;
  DJNZ MVGUARD_0          ;
  LD A,L                  ; Now A=Horace's screen y-coordinate (0-22).
  AND 31                  ;
  LD HL,(HORACELOC)       ; Pick up Horace's current location.
  LD H,A                  ; H=Horace's screen y-coordinate (0-22).
  LD A,L                  ; Now L=Horace's screen x-coordinate (0-30).
  AND 31                  ;
  LD L,A                  ;
  LD (TEMPHXY),HL         ; Save Horace's screen x- and y-coordinates
                          ; temporarily.
  LD HL,(GUARDLOC)        ; Compute the guard's screen x- and y-coordinates.
  RR H                    ;
  RR H                    ;
  RR H                    ;
  LD B,5                  ;
MVGUARD_1:
  RR H                    ;
  RR L                    ;
  DJNZ MVGUARD_1          ;
  LD A,L                  ;
  AND 31                  ;
  LD HL,(GUARDLOC)        ;
  LD H,A                  ;
  LD A,L                  ;
  AND 31                  ;
  LD L,A                  ;
  LD (TEMPXY),HL          ; Save the guard's screen x- and y-coordinates
                          ; temporarily.
  LD B,4                  ; Initialise B (the direction indicator) for the loop
                          ; that follows.
; Four passes are made through the following loop, one for each direction the
; guard might go: left, down, right, or up.
MVGUARD_2:
  DEC B                   ; B=3 (left), 2 (down), 1 (right) or 0 (up).
  LD HL,(GUARDLOC)        ; Pick up the guard's current location.
  LD C,0                  ; Initialise C to 0; this is the direction
                          ; probability parameter.
  PUSH BC                 ; Save the direction indicator briefly.
  LD A,B                  ; Copy the direction indicator to A.
  CALL CHKTILES           ; Check the tiles next to the guard in that
                          ; direction.
  POP BC                  ; Restore the direction indicator to B.
  CP 2                    ; Is there a wall or the maze exit or entrance in
                          ; that direction?
  JP NC,MVGUARD_14        ; Jump if so with C=0: that direction is blocked.
  LD C,25                 ; C=25; this value determines the base probability
                          ; that the guard will turn 90 degrees in the
                          ; direction indicated by B when his path is blocked.
  LD A,(GUARDAF)          ; Pick up the guard's animation frame.
  CP B                    ; Is it equal to the current value of the direction
                          ; indicator?
  JR NZ,MVGUARD_3         ; Jump if not.
  LD A,40                 ; C=65; this value ensures that the guard will keep
  ADD A,C                 ; moving in the same direction if he can.
  LD C,A                  ;
  JR MVGUARD_5
MVGUARD_3:
  ADD A,2                 ; Add 2 to the guard's current animation frame; this
  AND 3                   ; has the effect of toggling his direction between
                          ; up/down and left/right.
  CP B                    ; Is it equal to the current value of the direction
                          ; indicator now?
  JR NZ,MVGUARD_4         ; Jump if not.
  LD A,246                ; C=15; this value determines the probability that
  ADD A,C                 ; the guard will turn round 180 degrees when his path
  LD C,A                  ; is blocked.
  JR MVGUARD_5
MVGUARD_4:
  LD HL,(23672)           ; Increment the two least significant bytes of the
  INC HL                  ; system variable FRAMES, and copy the value to HL.
  LD (23672),HL           ;
  LD A,H                  ; Use this value to generate a pseudo-random number
  AND 15                  ; between 0 and 31.
  LD H,A                  ;
  LD A,(HL)               ;
  AND 31                  ;
  ADD A,C                 ; Add this to C, giving a number between 25 and 56;
  LD C,A                  ; the higher this value, the more likely the guard
                          ; will turn 90 degrees in the direction indicated by
                          ; B when his path is blocked.
MVGUARD_5:
  LD A,B                  ; Copy the direction indicator to A.
  CP 3                    ; Set the zero flag if we're considering 'left' at
                          ; the moment.
  LD A,(TEMPVAR)          ; Pick up the value of the redundant variable at
                          ; TEMPVAR.
  JR NZ,MVGUARD_6         ; Jump if we're considering 'right', 'up' or 'down'
                          ; at the moment.
  DEC A                   ; Decrement the redundant variable at TEMPVAR.
  LD (TEMPVAR),A          ;
MVGUARD_6:
  AND A                   ; This instruction is redundant.
  LD A,B                  ; Copy the direction indicator to A.
  CP 0                    ; Are we considering 'up' at the moment?
  JR NZ,MVGUARD_7         ; Jump if not.
  LD A,(SPEED)            ; Reset the redundant variable at TEMPVAR to the
  LD (TEMPVAR),A          ; value of the game speed parameter.
MVGUARD_7:
  LD A,B                  ; Copy the direction indicator to A.
  CP 0                    ; Are we considering 'up' at the moment?
  JR NZ,MVGUARD_8         ; Jump if not.
  LD HL,(TEMPXY)          ; Pick up the guard's screen x- and y-coordinates.
  LD A,H                  ; A=guard's screen y-coordinate.
  LD HL,(TEMPHXY)         ; Pick up Horace's screen x- and y-coordinates.
  SUB H                   ; Is Horace's y-coordinate greater than the guard's?
  JR C,MVGUARD_14         ; Jump if so.
  JR MVGUARD_11           ; Otherwise jump to increase the probability that the
                          ; guard will move up (towards Horace).
MVGUARD_8:
  CP 1                    ; Are we considering 'right' at the moment?
  JR NZ,MVGUARD_9         ; Jump if not.
  LD HL,(TEMPHXY)         ; Pick up Horace's screen x- and y-coordinates.
  LD A,L                  ; A=Horace's screen x-coordinate.
  LD HL,(TEMPXY)          ; Pick up the guard's screen x- and y-coordinates.
  SUB L                   ; Is the guard's x-coordinate greater than Horace's?
  JR C,MVGUARD_14         ; Jump if so.
  JR MVGUARD_11           ; Otherwise jump to increase the probability that the
                          ; guard will move right (towards Horace).
MVGUARD_9:
  CP 2                    ; Are we considering 'down' at the moment?
  JR NZ,MVGUARD_10        ; Jump if not.
  LD HL,(TEMPHXY)         ; Pick up Horace's screen x- and y-coordinates.
  LD A,H                  ; A=Horace's screen y-coordinate.
  LD HL,(TEMPXY)          ; Pick up the guard's screen x- and y-coordinates.
  SUB H                   ; Is the guard's y-coordinate greater than Horace's?
  JR C,MVGUARD_14         ; Jump if so.
  JR MVGUARD_11           ; Otherwise jump to increase the probability that the
                          ; guard will move down (towards Horace).
MVGUARD_10:
  LD HL,(TEMPXY)          ; Pick up the guard's screen x- and y-coordinates.
  LD A,L                  ; A=guard's screen x-coordinate.
  LD HL,(TEMPHXY)         ; Pick up Horace's screen x- and y-coordinates.
  SUB L                   ; Is Horace's x-coordinate greater than the guard's?
  JR C,MVGUARD_14         ; Jump if so.
MVGUARD_11:
  AND A                   ; This instruction is redundant.
  ADD A,10                ; Add 10 to A; the higher the value A holds now, the
                          ; more likely the guard will move in the direction
                          ; indicated by B (towards Horace).
  PUSH AF                 ; Save this probability modifier briefly.
  PUSH HL                 ; Save HL (unnecessarily).
  LD HL,(GUARDPT)         ; Pick up the guard panic timer.
  LD A,H                  ; Set the zero flag unless the guards are panicking.
  OR L                    ;
  POP HL                  ; Restore HL.
  JR Z,MVGUARD_12         ; Jump unless the guards are panicking.
  POP AF                  ; Restore the probability modifier to A.
  NEG                     ; Negate A, making it less likely that the guard will
                          ; move in the direction indicated by B (towards
                          ; Horace).
  JR MVGUARD_13
MVGUARD_12:
  POP AF                  ; Restore the probability modifier to A.
MVGUARD_13:
  ADD A,C                 ; Now C holds the probability parameter for the
  LD C,A                  ; direction indicated by B.
MVGUARD_14:
  PUSH BC                 ; Save the direction indicator briefly.
  LD HL,TEMPDPP           ; Point HL at one of the four slots at TEMPDPP.
  LD C,B                  ;
  LD B,0                  ;
  ADD HL,BC               ;
  POP BC                  ; Restore the direction indicator to B.
  LD A,C                  ; Save the direction probability parameter in the
  LD (HL),A               ; appropriate slot.
  INC B                   ; Have we considered every direction yet?
  DEC B                   ;
  JP NZ,MVGUARD_2         ; Jump back if not.
; Having computed a direction probability parameter for each of the four slots
; at TEMPDPP, we now use those values to determine the guard's next animation
; frame (and therefore direction of travel).
  XOR A                   ; Compute in C the index of the slot that holds the
  LD HL,28086             ; largest number (0, 1, 2 or 3).
  LD B,4                  ;
MVGUARD_15:
  DEC B                   ;
  DEC HL                  ;
  CP (HL)                 ;
  JR NC,MVGUARD_16        ;
  LD A,(HL)               ;
  LD C,B                  ;
MVGUARD_16:
  INC B                   ;
  DEC B                   ;
  JR NZ,MVGUARD_15        ;
  LD A,C                  ; Update the guard's animation frame to this index
  LD (GUARDAF),A          ; value.
  LD A,(GUARDAF)          ; Pick up the guard's animation frame.
  LD HL,(GUARDLOC)        ; Pick up the guard's current location.
  CALL CHKTILES           ; Check the tiles in front of the guard.
  CP 0                    ; Is there anything in front of the guard?
  JR Z,MVGUARD_19         ; Jump if not.
  CP 1                    ; Is the guard facing a tunnel entrance?
  JR NZ,MVGUARD_21        ; Jump if not. (This jump is never made.)
; The guard is about to enter a tunnel.
  LD BC,(CMTUNNEL)        ; Pick up the the tunnel offset.
  LD A,(GUARDAF)          ; Pick up the guard's animation frame.
  AND A                   ; Is it 0 (going up)?
  JR Z,MVGUARD_17         ; Jump if so.
  SBC HL,BC               ; Subtract the tunnel offset from the guard's current
                          ; location.
  JR MVGUARD_18
MVGUARD_17:
  ADD HL,BC               ; Add the tunnel offset to the guard's current
                          ; location.
MVGUARD_18:
  JR MVGUARD_20
MVGUARD_19:
  LD HL,(GUARDLOC)        ; Pick up the guard's current location.
  LD A,(GUARDAF)          ; Pick up the guard's animation frame.
  CALL INFRONT            ; Compute the guard's new location.
MVGUARD_20:
  LD (GUARDNLOC),HL       ; Update the guard's location.
  RET
; The guard has been left facing a wall or the maze entrance or exit (so he
; cannot move). This never happens, so the following code is never executed.
MVGUARD_21:
  LD HL,(GUARDLOC)        ; Pick up the guard's current location.
  LD (GUARDNLOC),HL       ; Set the guard's new location.
  RET

; Redraw the tiles behind Horace and the guards if they've moved
;
; Called from the main loop at MAINLOOP.
REDRAW:
  LD HL,(HORACENLOC)      ; Pick up Horace's new location.
  LD DE,(HORACELOC)       ; Pick up Horace's current location.
  XOR A                   ; Clear the carry flag.
  SBC HL,DE               ; Subtract Horace's current location from his new
                          ; location.
  LD A,H                  ; Has Horace moved?
  OR L                    ;
  JR Z,REDRAW_0           ; Jump if not.
  LD HL,(HORACELOC)       ; Pick up Horace's current location.
  LD DE,BLANK             ; Point DE at the graphic data for the blank sprite.
  CALL ATTRADDR           ; Set HL' to the attribute file address for Horace's
                          ; location.
  LD C,62                 ; This is the attribute byte for the blank sprite
                          ; (INK 6: PAPER 7).
  CALL DRAWSPRITE         ; Draw the blank sprite over Horace's current
                          ; location.
; Now consider the guards.
REDRAW_0:
  LD HL,GUARD4CT          ; Point HL at the last of the guard countdown timers.
  LD B,1                  ; B will count the guards: 1, 2, 3 and 4.
REDRAW_1:
  PUSH BC                 ; Save the guard counter.
  LD A,(HL)               ; Set the zero flag if this guard is in play.
  INC HL                  ;
  OR (HL)                 ;
  DEC HL                  ; Point HL at the next guard's countdown timer.
  DEC HL                  ;
  DEC HL                  ;
  PUSH HL                 ; Save the guard countdown timer pointer briefly.
  JR NZ,REDRAW_2          ; Jump if this guard is not in play yet.
  CALL COPYGUARD          ; Copy the guard's buffer to the temporary location
                          ; (GUARDLOC).
  CALL REDRAWG            ; Redraw the tiles behind the guard if he's moved.
  CALL UPDTGUARD          ; Copy the guard's buffer back to its original
                          ; location.
REDRAW_2:
  POP HL                  ; Restore the guard countdown timer pointer to HL.
  POP BC                  ; Restore the guard counter to B.
  INC B                   ; Next guard.
  LD A,B                  ; Copy the guard counter to A.
  CP 5                    ; Have we done all four guards yet?
  JR NZ,REDRAW_1          ; If not, jump back to do the next one.
  RET

; Redraw the tiles behind a guard after he's moved
;
; Used by the routine at REDRAW.
REDRAWG:
  LD HL,(GUARDNLOC)       ; Pick up the guard's new location.
  LD DE,(GUARDLOC)        ; Pick up the guard's current location.
  XOR A                   ; Clear the carry flag.
  SBC HL,DE               ; Subtract the guard's current location from his new
                          ; location.
  LD A,H                  ; Has the guard moved?
  OR L                    ;
  RET Z                   ; Return if not.
; This entry point is used by the routines at CHKHIT (when the guards have been
; thrown out of the park) and DEAD (after Horace has lost a life).
REDRAWG_0:
  LD HL,(GUARDLOC)        ; Pick up the guard's current location.
  LD BC,16384             ; Set HL to the corresponding display file address.
  ADD HL,BC               ;
  CALL ATTRADDR           ; Set HL' to the corresponding attribute file
                          ; address.
  LD DE,GUARDBG           ; Point DE at the first of the maze background tiles
                          ; in the guard's buffer.
  LD A,(DE)               ; Pick up the attribute byte of the first maze
                          ; background tile.
  INC DE                  ; Point DE at the graphic data for the first maze
                          ; background tile.
  LD C,A                  ; Copy the attribute byte to C.
  CALL PRINTTILE          ; Draw the first (top-left) maze background tile.
  LD A,(DE)               ; Pick up the attribute byte of the second maze
                          ; background tile.
  INC DE                  ; Point DE at the graphic data for the second maze
                          ; background tile.
  LD C,A                  ; Copy the attribute byte to C.
  CALL PRINTTILE          ; Draw the second (top-right) maze background tile.
  LD A,L                  ; Point HL at the third tile on-screen.
  ADD A,30                ;
  LD L,A                  ;
  JR NC,REDRAWG_1         ;
  LD A,H                  ;
  ADD A,8                 ;
  LD H,A                  ;
REDRAWG_1:
  LD A,(DE)               ; Pick up the attribute byte of the third maze
                          ; background tile.
  INC DE                  ; Point DE at the graphic data for the third maze
                          ; background tile.
  LD C,A                  ; Copy the attribute byte to C.
  CALL ATTRADDR           ; Set HL' to the appropriate attribute file address.
  CALL PRINTTILE          ; Draw the third (bottom-left) maze background tile.
  LD A,(DE)               ; Pick up the attribute byte of the fourth maze
                          ; background tile.
  INC DE                  ; Point DE at the graphic data for the fourth maze
                          ; background tile.
  LD C,A                  ; Copy the attribute byte to C.
  CALL PRINTTILE          ; Draw the fourth (bottom-right) maze background
                          ; tile.
  LD HL,(GUARDNLOC)       ; Pick up the guard's new location.
  CALL CPMAZEBG           ; Copy the maze backround tiles at this location into
                          ; the guard's buffer.
  RET

; Add to the score if Horace has eaten something
;
; Called from the main loop at MAINLOOP.
CHKEATEN:
  LD A,(TUNNELTIME)       ; Pick up the tunnel timer.
  AND A                   ; Is Horace in a tunnel?
  RET NZ                  ; Return if so.
  LD HL,(HORACENLOC)      ; Pick up Horace's new location.
  CALL CHKATTRS           ; Check the attribute bytes at this location.
  RET Z                   ; Return if there's nothing to eat there.
  CP 60                   ; Has Horace run into a flower?
  JR NZ,CHKEATEN_0        ; Jump if not.
  LD HL,(SCORE)           ; Add 10 to the score (Horace has eaten a flower).
  LD BC,10                ;
  ADD HL,BC               ;
  LD (SCORE),HL           ;
  JR CHKEATEN_1
CHKEATEN_0:
  LD HL,(SCORE)           ; Add 50 to the score (Horace has eaten a cherry or
  LD BC,50                ; strawberry).
  ADD HL,BC               ;
  LD (SCORE),HL           ;
; This entry point is used by the routines at START (when initialising a maze),
; CHKHIT (when Horace has thrown the guards out of the park) and DRAWBELL (when
; Horace has sounded the alarm).
CHKEATEN_1:
  LD DE,16384             ; Set DE to the display file address for the score
  EX DE,HL                ; (in a roundabout way).
  LD BC,15                ;
  ADD HL,BC               ;
  EX DE,HL                ;
  CALL PRINTSCORE         ; Print the score.
  LD HL,(SCORE)           ; Pick up the current score.
  XOR A                   ; Clear the carry flag and set A=0.
  LD BC,10000             ; Perform trial subtractions of 10000 from the score.
CHKEATEN_2:
  INC A                   ;
  SBC HL,BC               ;
  JR NC,CHKEATEN_2        ;
  INC A                   ; This should be DEC A.
  LD B,A                  ; Now B=INT(Score/10000)+2.
  LD A,(ELIFEIND)         ; Pick up the extra life indicator.
  CP B                    ; Is it time to award an extra life?
  JR Z,CHKEATEN_3         ; Jump if not.
  LD A,B                  ; Update the extra life indicator.
  LD (ELIFEIND),A         ;
  LD A,(LIVES)            ; Increment the number of lives.
  INC A                   ;
  LD (LIVES),A            ;
  LD HL,16391             ; This is the display file address for the number of
                          ; lives.
  CALL ATTRADDR           ; Set HL' to the corresponding attribute file address
                          ; (unnecessarily).
  LD A,(LIVES)            ; Pick up the number of lives remaining.
  ADD A,"0"               ; Convert it into an ASCII code. This does not work
                          ; if there are 10 or more lives remaining, which is a
                          ; bug.
  EX DE,HL                ; Transfer the display file address to DE.
  CALL PRINTDIGIT_0       ; Print the number of lives remaining.
CHKEATEN_3:
  LD HL,SOUND             ; Pick up the sound on/off indicator.
  LD C,16                 ; Initialise C for the loop that follows.
CHKEATEN_4:
  PUSH BC                 ; Save the loop counter (unnecessarily).
  LD B,C                  ; Set B equal to 8*C. This value determines the
  SLA B                   ; pitch.
  SLA B                   ;
  SLA B                   ;
  PUSH BC                 ; Save the pitch parameter briefly.
  LD A,31                 ; Flip the speaker off if the sound on/off indicator
  AND (HL)                ; is on, or on otherwise.
  OUT (254),A             ;
CHKEATEN_5:
  DJNZ CHKEATEN_5         ; Produce a short delay.
  LD A,7                  ; Flip the speaker on.
  OUT (254),A             ;
  POP BC                  ; Restore the pitch parameter to B.
CHKEATEN_6:
  DJNZ CHKEATEN_6         ; Produce another short delay.
  POP BC                  ; Restore the loop counter to C (unnecessarily).
  DEC C                   ; Finished yet?
  JR NZ,CHKEATEN_4        ; Jump back if not.
  RET

; Draw the guards
;
; Called from the main loop at MAINLOOP.
DRAWGUARDS:
  LD HL,GUARD1CT          ; Point HL at the first of the guard countdown
                          ; timers.
  LD B,4                  ; There are four guards.
DRAWGUARDS_0:
  PUSH BC                 ; Save the guard counter.
  LD A,(HL)               ; Set the zero flag if this guard is in play.
  INC HL                  ;
  OR (HL)                 ;
  INC HL                  ; Point HL at the next guard's countdown timer.
  PUSH HL                 ; Save the guard countdown timer pointer.
  JR NZ,DRAWGUARDS_1      ; Jump if this guard is not in play yet.
  CALL COPYGUARD          ; Copy the guard's buffer into the temporary location
                          ; (GUARDLOC).
  CALL DRAWGUARD          ; Draw the guard.
  CALL UPDTGUARD          ; Copy the guard's buffer back to the original
                          ; location.
DRAWGUARDS_1:
  POP HL                  ; Restore the guard countdown timer pointer to HL.
  POP BC                  ; Restore the guard counter to B.
  DJNZ DRAWGUARDS_0       ; Jump back to deal with the next guard.
  RET

; Draw a guard
;
; Used by the routine at DRAWGUARDS.
DRAWGUARD:
  LD A,(GUARDDELAY)       ; Pick up the guard's return delay counter.
  AND A                   ; Has this guard been thrown out of the park?
  JR Z,DRAWGUARD_0        ; Jump if not.
  DEC A                   ; Decrement the guard's return delay counter.
  LD (GUARDDELAY),A       ;
  RET NZ                  ; Return unless it's zero now.
DRAWGUARD_0:
  LD HL,(GUARDNLOC)       ; Update the guard's current location.
  LD (GUARDLOC),HL        ;
  LD BC,(GUARDPT)         ; Pick up the guard panic timer.
  LD A,B                  ; Are the guards panicking?
  OR C                    ;
  JR Z,DRAWGUARD_2        ; Jump if not.
  DEC BC                  ; Decrement the guard panic timer.
  LD (GUARDPT),BC         ;
  LD DE,32455             ; Point HL at the graphic data for a panicked guard
  EX DE,HL                ; (frame 0).
  LD A,(GUARDTIMER)       ; Increment the guard's animation frame timer.
  INC A                   ;
  LD (GUARDTIMER),A       ;
  AND 8                   ; Is bit 3 set now?
  CP 0                    ;
  JR Z,DRAWGUARD_1        ; Jump if not.
  LD BC,32                ; Point DE at the graphic data for a panicked guard
  ADD HL,BC               ; (frame 1), and restore the guard's location to HL.
DRAWGUARD_1:
  EX DE,HL                ;
  JR DRAWGUARD_5          ; Jump forward to draw the guard now.
DRAWGUARD_2:
  EX DE,HL                ; Transfer the guard's location to DE.
  LD A,(GUARDTIMER)       ; Increment the guard's animation frame timer.
  INC A                   ;
  LD (GUARDTIMER),A       ;
  AND 32                  ; Is bit 5 set now?
  CP 0                    ;
  JR NZ,DRAWGUARD_3       ; Jump if so.
  LD HL,32327             ; Point HL at the graphic data for a regular guard
                          ; (frame 4).
  JR DRAWGUARD_4
DRAWGUARD_3:
  LD HL,GUARD0            ; Point HL at the graphic data for a regular guard
                          ; (frame 0).
DRAWGUARD_4:
  LD A,(GUARDAF)          ; Pick up the guard's animation frame.
  RLC A                   ; Point DE at the corresponding graphic data for the
  RLC A                   ; guard sprite, and restore the guard's location to
  RLC A                   ; HL.
  RLC A                   ;
  RLC A                   ;
  LD B,0                  ;
  LD C,A                  ;
  ADD HL,BC               ;
  EX DE,HL                ;
DRAWGUARD_5:
  CALL ATTRADDR           ; Set HL' to the attribute file address of the
                          ; guard's location.
  LD C,59                 ; This is the guard's attribute byte (INK 3: PAPER
                          ; 7).
  CALL DRAWSPRITE         ; Draw the guard.
  RET

; Check whether Horace has run into a guard
;
; Called from the main loop at MAINLOOP.
CHKHIT:
  LD HL,(HORACENLOC)      ; Pick up Horace's new location.
  CALL CHKATTRS           ; Check the attribute bytes at Horace's new location.
  CP 59                   ; Has Horace run into a guard?
  RET NZ                  ; Return if not.
  LD HL,(GUARDPT)         ; Pick up the guard panic timer.
  LD A,H                  ; Are the guards panicking at the moment?
  OR L                    ;
  JP Z,DEAD               ; Jump if not.
  LD HL,(SCORE)           ; Add 100 to the score (Horace has thrown the guards
  LD BC,100               ; out of the park).
  ADD HL,BC               ;
  LD (SCORE),HL           ;
  CALL CHKEATEN_1         ; Print the score.
  LD HL,0                 ; Reset the guard panic timer.
  LD (GUARDPT),HL         ;
  LD HL,SOUND             ; Pick up the sound on/off indicator.
  LD C,15                 ; Initialise C for the loop that follows.
CHKHIT_0:
  PUSH BC                 ; Save the loop counter (unnecessarily).
  LD A,C                  ; Set B and E equal to 8*(15-C). This value
  XOR 15                  ; determines the pitch.
  LD B,A                  ;
  SLA B                   ;
  SLA B                   ;
  SLA B                   ;
  LD E,B                  ;
  LD D,8                  ; This value determines the duration.
CHKHIT_1:
  LD B,E                  ; Produce a sound (if the sound on/off indicator is
  LD A,31                 ; on) with pitch and duration determined by E and D.
  AND (HL)                ;
  OUT (254),A             ;
CHKHIT_2:
  LD A,(IX+0)             ;
  LD (IX+0),A             ;
  DJNZ CHKHIT_2           ;
  LD A,7                  ;
  OUT (254),A             ;
  LD B,E                  ;
CHKHIT_3:
  LD A,(IX+0)             ;
  LD (IX+0),A             ;
  DJNZ CHKHIT_3           ;
  DEC D                   ;
  JR NZ,CHKHIT_1          ;
  POP BC                  ; Restore the loop counter to C (unnecessarily).
  DEC C                   ; Finished yet?
  JR NZ,CHKHIT_0          ; Jump back if not.
; Now we throw the guards out of the park.
  LD HL,GUARD1CT          ; Point HL at the first guard countdown timer.
  LD B,4                  ; There are four guards.
CHKHIT_4:
  PUSH BC                 ; Save the guard counter.
  LD A,(HL)               ; Set the zero flag if this guard is already in play.
  INC HL                  ;
  OR (HL)                 ;
  PUSH HL                 ; Save the guard countdown timer pointer.
  JR NZ,CHKHIT_6          ; Jump if this guard has not appeared yet.
  CALL COPYGUARD          ; Copy this guard's buffer into the temporary
                          ; location (GUARDLOC).
  CALL REDRAWG_0          ; Redraw the tiles behind the guard.
  LD HL,(HORACENLOC)      ; Pick up Horace's new location.
  CALL CHKATTRS           ; Check the attribute bytes at this location.
  CP 59                   ; Set the zero flag if there's a guard here. (This
                          ; instruction is redundant.)
  LD HL,(CMENTRANCE)      ; Pick up the entrance location for the current maze.
  LD (GUARDLOC),HL        ; Make this the guard's new location.
  LD (GUARDNLOC),HL       ;
  CALL CPMAZEBG           ; Copy the maze background tiles into the temporary
                          ; guard buffer.
  LD A,1                  ; Initialise the guard's animation frame.
  LD (GUARDAF),A          ;
  LD A,90                 ; Initialise the guard's return delay counter.
  LD (GUARDDELAY),A       ;
  JR CHKHIT_5
  CALL DRAWGUARD          ; Draw the guard (this instruction is never
                          ; executed).
CHKHIT_5:
  CALL UPDTGUARD          ; Copy the guard's buffer back to the original
                          ; location.
CHKHIT_6:
  POP HL                  ; Restore the guard countdown timer pointer to HL.
  POP BC                  ; Restore the guard counter to B.
  DJNZ CHKHIT_4           ; Jump back to deal with the next guard.
  RET

; Lose a life
;
; Used by the routine at CHKHIT.
DEAD:
  LD A,(LIVES)            ; Decrement the number of lives.
  DEC A                   ;
  LD (LIVES),A            ;
  XOR A                   ; Reset the tunnel timer.
  LD (TUNNELTIME),A       ;
  LD (GUARDDELAY),A       ; Reset the current guard's return delay counter.
  LD HL,16391             ; This is the display file address for the score.
  CALL ATTRADDR           ; Set HL' to the corresponding attribute file address
                          ; (unnecessarily).
  LD A,(LIVES)            ; Pick up the number of lives remaining.
  ADD A,"0"               ; Convert it to the ASCII code of the digit.
  EX DE,HL                ; Transfer the display file address to DE.
  CALL PRINTDIGIT_0       ; Print the number of lives remaining.
; The following loop produces the Horace-has-died colour-cycling effect.
  LD B,45                 ; Initialise the loop counter.
DEAD_0:
  PUSH BC                 ; Save the loop counter.
  LD A,(HORACEATTR)       ; Increment Horace's INK colour.
  INC A                   ;
  AND 7                   ;
  OR 56                   ;
  LD (HORACEATTR),A       ;
  PUSH AF                 ; Save the attribute byte briefly.
  CALL DRAWHORACE_0       ; Draw Horace in this new colour.
  LD HL,22528             ; This is the attribute file address for the 'P' of
                          ; 'PASSES'.
  LD B,9                  ; B will count the characters in 'PASSES n ' (where
                          ; 'n' in the number of lives remaining).
  POP AF                  ; Restore the attribute byte to A.
DEAD_1:
  LD (HL),A               ; Change the INK colour of the number of remaining
  INC HL                  ; lives to match that of Horace.
  DJNZ DEAD_1             ;
  LD C,20                 ; Initialise C for the loop that follows.
DEAD_2:
  LD A,31                 ; Flip the speaker off if the sound on/off indicator
  LD HL,SOUND             ; is on, or on otherwise.
  AND (HL)                ;
  OUT (254),A             ;
  CALL RANDOM             ; Generate a pseudo-random number in A.
  OR 64                   ; Set bit 6 to make sure it's in the range 64-255.
  LD B,A                  ; Use this value to produce a short delay.
DEAD_3:
  DJNZ DEAD_3             ;
  LD A,7                  ; Flip the speaker on.
  OUT (254),A             ;
  CALL RANDOM             ; Generate another pseudo-random number in the range
  OR 64                   ; 64-255.
  LD B,A                  ; Use this value to produce a short delay.
DEAD_4:
  DJNZ DEAD_4             ;
  DEC C                   ; Finished yet?
  JR NZ,DEAD_2            ; Jump back if not.
  POP BC                  ; Restore the loop counter to B.
  DJNZ DEAD_0             ; Jump back for the next iteration.
; Now that's done, reinitialise Horace and the guards.
  LD A,56                 ; Reset the INK colour of the number of remaining
  LD HL,22528             ; lives to black.
  LD B,8                  ;
DEAD_5:
  LD (HL),A               ;
  INC HL                  ;
  DJNZ DEAD_5             ;
  LD A,57                 ; Reset Horace's attribute byte (INK 1: PAPER 7).
  LD (HORACEATTR),A       ;
  LD HL,GUARD4CT          ; Point HL at the last of the four guard countdown
                          ; timers.
  LD B,1                  ; B will count four guards (1, 2, 3, 4).
DEAD_6:
  PUSH BC                 ; Save the guard counter.
  LD A,(HL)               ; Set the zero flag if this guard is in play.
  INC HL                  ;
  OR (HL)                 ;
  DEC HL                  ; Point HL at the next guard's countdown timer.
  DEC HL                  ;
  DEC HL                  ;
  PUSH HL                 ; Save the guard countdown timer pointer.
  JR NZ,DEAD_7            ; Jump if this guard is not in play yet.
  CALL COPYGUARD          ; Copy the guard's buffer into the temporary location
                          ; (GUARDLOC).
  CALL REDRAWG_0          ; Redraw the maze background tiles at this guard's
                          ; location.
  CALL UPDTGUARD          ; Copy the guard's buffer back to the original
                          ; location.
DEAD_7:
  POP HL                  ; Restore the guard countdown timer pointer to HL.
  POP BC                  ; Restore the guard counter to B.
  INC B                   ; Next guard.
  LD A,B                  ; Copy the guard counter to A.
  CP 5                    ; Have we done all four guards yet?
  JR NZ,DEAD_6            ; If not, jump back to do the next one.
  LD HL,(HORACELOC)       ; Pick up Horace's current location.
  LD DE,BLANK             ; Point DE at the graphic data for the blank sprite.
  CALL ATTRADDR           ; Set HL' to the attribute file address corresponding
                          ; to Horace's location.
  LD C,62                 ; This is the attribute byte for the blank sprite
                          ; (INK 6: PAPER 7).
  CALL DRAWSPRITE         ; Draw the blank sprite at Horace's location.
  LD A,(LIVES)            ; Pick up the number of lives remaining.
  AND A                   ; Is it zero?
  JR Z,DEAD_8             ; Jump if so.
  POP HL                  ; Drop the return address from the stack.
  CALL INITGCT            ; Initialise the guard countdown timers.
  LD A,(GAMEMODE)         ; Pick up the game mode indicator.
  AND A                   ; Is it demo mode?
  JP NZ,START_18          ; Jump if not.
  JP START_5              ; Otherwise move to the next maze.
; Horace has just lost his last remaining life. This entry point is also used
; by the routine at READKEYS when a key is pressed in demo mode.
DEAD_8:
  LD B,20                 ; This loop will have 20 iterations.
DEAD_9:
  PUSH BC                 ; Save the loop counter.
  CALL TITLESOUND         ; Make a sound effect.
  LD HL,22528             ; Change the INK colour of the entire screen to B mod
  LD DE,22529             ; 8.
  LD A,B                  ;
  AND 7                   ;
  ADD A,56                ;
  LD (HL),A               ;
  LD BC,767               ;
  LDIR                    ;
  POP BC                  ; Restore the loop counter to B.
  DJNZ DEAD_9             ; Jump back for the next iteration.
  POP HL                  ; Drop the return address from the stack.
  LD A,(GAMEMODE)         ; Pick up the game mode indicator.
  AND A                   ; Is it demo mode?
  JP Z,START_0            ; Return to the title screen if so.
  LD HL,(SCORE)           ; Pick up the current score.
  LD BC,(HISCORE)         ; Pick up the high score.
  XOR A                   ; Clear the carry flag for subtraction.
  SBC HL,BC               ; Do we have a new high score?
  JP C,START_0            ; Return to the title screen if not.
  LD HL,(SCORE)           ; Pick up the current score.
  LD (HISCORE),HL         ; Make it the new high score.
  JP START_0              ; Return to the title screen.

; Draw Horace
;
; Called from the main loop at MAINLOOP, and also used by the routine at
; DRAWBELL.
DRAWHORACE:
  LD A,(TUNNELTIME)       ; Pick up the tunnel timer.
  AND A                   ; Is Horace in a tunnel at the moment?
  JR Z,DRAWHORACE_0       ; Jump if not.
  DEC A                   ; Decrement the tunnel timer.
  LD (TUNNELTIME),A       ;
  LD HL,(HORACENLOC)      ; Update Horace's current location.
  LD (HORACELOC),HL       ;
  RET
; This entry point is used by the routine at DEAD to draw Horace during the
; Horace-has-died colour-cycling effect.
DRAWHORACE_0:
  LD A,(HORACETIME)       ; Increment Horace's walking animation timer.
  INC A                   ;
  LD (HORACETIME),A       ;
  AND 16                  ; Keep only bit 4.
  CP 0                    ; Is bit 4 set now? (This instruction is redundant.)
  JR NZ,DRAWHORACE_1      ; Jump if so.
  LD HL,32071             ; Point HL at the graphic data for Horace's sprite
                          ; (frame 4).
  JR DRAWHORACE_2
DRAWHORACE_1:
  LD HL,HORACE0           ; This is the base address of the graphic data for
                          ; Horace's sprite (frame 0).
DRAWHORACE_2:
  LD A,(HORACEAF)         ; Pick up Horace's animation frame in A.
  RLC A                   ; Point DE at the graphic data for the corresponding
  RLC A                   ; sprite (HL+32*A).
  RLC A                   ;
  RLC A                   ;
  RLC A                   ;
  LD B,0                  ;
  LD C,A                  ;
  ADD HL,BC               ;
  EX DE,HL                ;
  LD HL,(HORACENLOC)      ; Update Horace's current location.
  LD (HORACELOC),HL       ;
  CALL ATTRADDR           ; Set HL' to the corresponding attribute file
                          ; address.
  LD A,(HORACEATTR)       ; Pick up Horace's attribute byte and copy it to C.
  LD C,A                  ;
  CALL DRAWSPRITE         ; Draw Horace.
  RET

; Draw the bell and check whether Horace has sounded the alarm
;
; Called from the main loop at MAINLOOP.
DRAWBELL:
  LD HL,(HORACELOC)       ; Pick up Horace's current location.
  LD BC,(BELLLOC)         ; Pick up the location of the bell in the current
                          ; maze.
  LD A,B                  ; Has Horace already sounded the alarm?
  OR C                    ;
  RET Z                   ; Return if so.
  AND A                   ; Clear the carry flag. (This instruction is
                          ; redundant.)
  SBC HL,BC               ; Subtract the bell's location from Horace's.
  LD A,H                  ; Is Horace in exactly the same location as the bell?
  OR L                    ;
  JR Z,DRAWBELL_1         ; Jump if so.
  INC HL                  ; Is Horace one space to the left of the bell?
  LD A,H                  ;
  OR L                    ;
  JR Z,DRAWBELL_1         ; Jump if so.
  DEC HL                  ; Is Horace one space to the right of the bell?
  DEC HL                  ;
  LD A,H                  ;
  OR L                    ;
  JR Z,DRAWBELL_1         ; Jump if so.
  LD BC,33                ; Is Horace one space above the bell?
  ADD HL,BC               ;
  LD A,H                  ;
  OR L                    ;
  JR Z,DRAWBELL_1         ; Jump if so.
  XOR A                   ; Clear the carry flag. (This instruction is
                          ; redundant.)
  LD BC,65                ; Is Horace one space below the bell?
  SBC HL,BC               ;
  LD A,H                  ;
  OR L                    ;
  JR Z,DRAWBELL_1         ; Jump if so.
; Horace has not sounded the alarm.
  LD HL,(BELLLOC)         ; Pick up the location of the bell in the current
                          ; maze.
  LD A,(BELLANIMFC)       ; Increment the bell's animation frame counter.
  INC A                   ;
  LD (BELLANIMFC),A       ;
  AND 56                  ; Keep only bits 3, 4 and 5.
  CP 31                   ; Is the result 30 or less (i.e. bit 5 reset)?
  JR C,DRAWBELL_0         ; Jump if so.
  XOR 56                  ; Flip bits 3 and 4, and reset bit 5.
DRAWBELL_0:
  RLCA                    ; BC=4*A.
  RLCA                    ;
  LD C,A                  ;
  LD B,0                  ;
  LD DE,BELL              ; This is the base address of the graphic data for
                          ; the bell.
  EX DE,HL                ; Point DE at the graphic data for the appropriate
  ADD HL,BC               ; bell sprite.
  EX DE,HL                ;
  CALL ATTRADDR           ; Set HL' to the attribute file address of the bell's
                          ; location.
  LD C,58                 ; This is the attribute byte (INK 2: PAPER 7).
  CALL DRAWSPRITE         ; Draw the bell.
  RET
; Horace has sounded the alarm.
DRAWBELL_1:
  LD HL,0                 ; Clear the bell location to indicate that Horace has
  LD (BELLLOC),HL         ; sounded the alarm.
  LD DE,BLANK             ; Point DE at the graphic data for the blank sprite.
  LD HL,(CMBELLLOC)       ; Pick up the bell location for the current maze.
  CALL ATTRADDR           ; Set HL' to the corresponding attribute file
                          ; address.
  LD C,62                 ; This is the attribute byte (INK 6: PAPER 7).
  CALL DRAWSPRITE         ; Draw the blank sprite where the bell was.
  CALL DRAWHORACE         ; Draw Horace.
  LD HL,(SCORE)           ; Add 60 to the score.
  LD BC,60                ;
  ADD HL,BC               ;
  LD (SCORE),HL           ;
  CALL CHKEATEN_1         ; Print the new score and make a sound effect.
  LD A,(SPEED)            ; Pick up the game speed parameter in A.
  LD L,0                  ; HL=128*A.
  LD H,A                  ;
  SRA H                   ;
  RR L                    ;
  LD (GUARDPT),HL         ; Initialise the guard panic timer to this value.
  LD A,(GUARDAF)          ; Update the animation frame in the temporary guard
  ADD A,2                 ; buffer; these instructions are redundant and have
  AND 3                   ; no effect on any of the guards.
  LD (GUARDAF),A          ;
  LD HL,(SCORE)           ; Add 150 to the score.
  LD BC,150               ;
  ADD HL,BC               ;
  LD (SCORE),HL           ;
  CALL CHKEATEN_1         ; Print the new score and make a sound effect.
  RET

; Check whether a guard should drop his lunch
;
; Called from the main loop at MAINLOOP.
CHKLUNCH:
  LD HL,GUARD1CT          ; Point HL at the first guard countdown timer.
  LD B,4                  ; There are four guards to consider.
CHKLUNCH_0:
  PUSH BC                 ; Save the guard counter.
  LD A,(HL)               ; Set the zero flag if this guard is in play.
  INC HL                  ;
  OR (HL)                 ;
  INC HL                  ; Point HL at the next guard's countdown timer.
  PUSH HL                 ; Save the guard countdown timer pointer.
  JR NZ,CHKLUNCH_1        ; Jump if this guard is not in play yet.
  CALL COPYGUARD          ; Copy the guard's buffer into the temporary location
                          ; (GUARDLOC).
  CALL DROPLUNCH          ; Make the guard drop his lunch if necessary.
  CALL UPDTGUARD          ; Copy the guard's buffer back to the original
                          ; location.
CHKLUNCH_1:
  POP HL                  ; Restore the guard countdown timer pointer to HL.
  POP BC                  ; Restore the guard counter to B.
  DJNZ CHKLUNCH_0         ; Jump back to handle the next guard.
  RET

; Make a guard drop his lunch if necessary
;
; Used by the routine at CHKLUNCH.
DROPLUNCH:
  LD A,(GUARDDELAY)       ; Pick up the guard's return delay counter.
  AND A                   ; Has this guard been thrown out of the park?
  RET NZ                  ; Return if so.
  LD E,4                  ; There are four maze background tiles to consider.
  LD HL,GUARDBG           ; Point HL at the first maze background tile in the
                          ; temporary guard buffer.
DROPLUNCH_0:
  LD A,(HL)               ; Pick up the attribute byte of this maze background
                          ; tile.
  CP 58                   ; Is there already a cherry or strawberry here?
  RET Z                   ; Return if so.
  LD BC,9                 ; Point HL at the next maze background tile.
  ADD HL,BC               ;
  DEC E                   ; Have we checked all four tiles yet?
  JR NZ,DROPLUNCH_0       ; Jump back if not.
  LD HL,(LUNCHCT)         ; Decrement the lunch-drop countdown timer.
  DEC HL                  ;
  LD (LUNCHCT),HL         ;
  LD A,H                  ; Is it zero now?
  OR L                    ;
  RET NZ                  ; Return if not.
  CALL RANDOM             ; Generate a pseudo-random number in A.
  LD L,A                  ; Copy it to L.
  PUSH HL                 ; Save L briefly.
  CALL RANDOM             ; Generate another pseudo-random number in A.
  POP HL                  ; Restore L.
  AND 3                   ; Reduce A to 0, 1, 2 or 3 and copy it to H.
  LD H,A                  ;
  LD BC,800               ; Add 800 and reset the lunch-drop countdown timer to
  ADD HL,BC               ; this value.
  LD (LUNCHCT),HL         ;
  CALL RANDOM             ; Generate yet another pseudo-random number in A.
  AND 1                   ; Is bit 0 set?
  JR NZ,DROPLUNCH_1       ; Jump if so.
  LD HL,CHERRY            ; Point HL at the sprite data for the cherry.
  JR DROPLUNCH_2
DROPLUNCH_1:
  LD HL,STRAWBERRY        ; Point HL at the sprite data for the strawberry.
DROPLUNCH_2:
  LD DE,GUARDBG           ; Copy the cherry/strawberry sprite into the maze
  LD BC,36                ; background tiles in the temporary guard buffer.
  LDIR                    ;
  RET

; Decrement the guard countdown timers
;
; Called from the main loop at MAINLOOP.
DECGTIMERS:
  LD HL,GUARD1CT          ; Point HL at the first guard countdown timer.
  LD B,4                  ; There are four guards to consider.
DECGTIMERS_0:
  LD E,(HL)               ; Pick up the countdown timer value in DE.
  INC HL                  ;
  LD D,(HL)               ;
  DEC HL
  LD A,D                  ; Is the value zero?
  OR E                    ;
  JR Z,DECGTIMERS_1       ; Jump if so (this guard is already in play).
  DEC DE                  ; Decrement the countdown timer.
  LD (HL),E               ;
  INC HL                  ;
  LD (HL),D               ;
  DEC HL
  LD A,D
  OR C                    ; This should be OR E.
  JR NZ,DECGTIMERS_1      ; Jump unless A is now zero.
  LD A,(GUARDCOUNT)       ; Increment the active guard counter.
  INC A                   ;
  LD (GUARDCOUNT),A       ;
DECGTIMERS_1:
  INC HL                  ; Point HL at the next guard's countdown timer.
  INC HL                  ;
  DJNZ DECGTIMERS_0       ; Jump back to consider the next guard.
  RET

; Toggle the sound on/off if 'T' is pressed
;
; Called from the main loop at MAINLOOP.
TOGGLESND:
  LD A,251                ; Read keys Q-W-E-R-T.
  IN A,(254)              ;
  AND 16                  ; Keep only bit 4 (corresponding to 'T').
  LD HL,TPRESS            ; Pick up the last recorded 'T' pressed indicator.
  CP (HL)                 ; Does the current value match?
  RET Z                   ; Return if so.
  LD (HL),A               ; Save the current 'T' pressed indicator.
  AND A                   ; Is 'T' being pressed?
  RET NZ                  ; Return if not.
  LD HL,SOUND             ; Toggle the sound on/off indicator by flipping bits
  LD A,(HL)               ; 3 and 4.
  XOR 24                  ;
  LD (HL),A               ;
  RET

; Ring the bell if necessary
;
; Called from the main loop at MAINLOOP.
RINGBELL:
  LD HL,(BELLLOC)         ; Pick up the bell's location in the current maze.
  LD A,H                  ; Has Horace already sounded the alarm?
  OR L                    ;
  JR NZ,RINGBELL_2        ; Jump if not.
; Horace has already sounded the alarm. Produce an appropriate delay in place
; of the bell sound.
RINGBELL_0:
  LD A,(SPEED)            ; Pick up the game speed parameter in A.
  LD E,A                  ; HL=350+30*A.
  LD HL,350               ;
RINGBELL_1:
  LD BC,30                ;
  ADD HL,BC               ;
  DEC E                   ;
  JR NZ,RINGBELL_1        ;
  XOR A                   ; Clear A for no apparent reason.
  LD A,(GUARDCOUNT)       ; Pick up the active guard counter (0-3) in A.
  RLA                     ; Subtract 128*A from HL.
  RLA                     ;
  RLA                     ;
  RLA                     ;
  RLA                     ;
  RLA                     ;
  LD B,0                  ;
  LD C,A                  ;
  SBC HL,BC               ;
  SBC HL,BC               ;
  PUSH HL                 ; Copy HL to BC.
  POP BC                  ;
  CALL DELAY              ; Wait for 26*BC+5 T states.
  RET
; Horace has not sounded the alarm yet. Produce an appropriate bell sound if
; necessary.
RINGBELL_2:
  LD A,(BELLANIMFC)       ; Pick up the bell animation frame counter.
  AND 63                  ; Keep only bits 0-5.
  CP 0                    ; Is the frame counter a multiple of 64 at the
                          ; moment? (This instruction is redundant.)
  JR NZ,RINGBELL_3        ; Jump if not.
  LD HL,SOUND             ; Pick up the sound on/off indicator in A.
  LD A,(HL)               ;
  CP 31                   ; Is the sound on?
  JR NZ,RINGBELL_0        ; Jump if not.
  LD DE,22                ; Call the ROM to make a short sound effect.
  LD HL,403               ;
  CALL 949                ;
  DI                      ; Disable interrupts after the ROM call.
  LD DE,26                ; Call the ROM again to make another short sound
  LD HL,360               ; effect.
  CALL 949                ;
  DI                      ; Disable interrupts after the ROM call.
  RET
RINGBELL_3:
  CP 32                   ; Is the bell animation frame counter a multiple of
                          ; 32 at the moment?
  JR NZ,RINGBELL_0        ; Jump if not.
  LD HL,SOUND             ; Pick up the sound on/off indicator in A.
  LD A,(HL)               ;
  CP 31                   ; Is the sound on?
  JR NZ,RINGBELL_0        ; Jump if not.
  LD DE,26                ; Call the ROM to make a short sound effect.
  LD HL,360               ;
  CALL 949                ;
  DI                      ; Disable interrupts after the ROM call.
  LD DE,22                ; Call the ROM again to make another short sound
  LD HL,403               ; effect.
  CALL 949                ;
  DI                      ; Disable interrupts after the ROM call.
  RET

; Unused
  LD A,(IX+0)
  LD A,(IX+0)
  RET

; Enter the next maze
;
; Used by the routine at MVSPRITES when Horace is leaving the current maze.
NEXTMAZE:
  POP HL                  ; Drop the return address from the stack.
  JP START_5              ; Prepare the next maze.

; Initialise the guard countdown timers
;
; Used by the routines at START and DEAD.
INITGCT:
  LD A,0                  ; Reset the active guard counter.
  LD (GUARDCOUNT),A       ;
  LD HL,0                 ; Initialise the first guard countdown timer to zero.
  LD (GUARD1CT),HL        ;
  LD A,(SPEED)            ; Pick up the game speed parameter (1-8) in A.
  LD H,A                  ; HL=256*A.
  LD (GUARD2CT),HL        ; Initialise the second guard countdown timer.
  SLA H                   ; Double HL.
  LD (GUARD3CT),HL        ; Initialise the third guard countdown timer.
  SLA H                   ; Double HL again.
  LD (GUARD4CT),HL        ; Initialise the fourth guard countdown timer.
  RET

; Copy a guard buffer into the temporary location
;
; Used by the routines at START, MVSPRITES, REDRAW, DRAWGUARDS, CHKHIT, DEAD
; and CHKLUNCH.
;
; B Guard number (1-4)
COPYGUARD:
  LD A,B                  ; Copy the guard number (1, 2, 3, 4) to A.
  CP 4                    ; Are we dealing with guard 4?
  JR NZ,COPYGUARD_0       ; Jump if not.
  LD HL,GUARD1BUF         ; Point HL at the buffer for guard 4.
  JR COPYGUARD_3
COPYGUARD_0:
  CP 3                    ; Are we dealing with guard 3?
  JR NZ,COPYGUARD_1       ; Jump if not.
  LD HL,GUARD2BUF         ; Point HL at the buffer for guard 3.
  JR COPYGUARD_3
COPYGUARD_1:
  CP 2                    ; Are we dealing with guard 2?
  JR NZ,COPYGUARD_2       ; Jump if not.
  LD HL,GUARD3BUF         ; Point HL at the buffer for guard 2.
  JR COPYGUARD_3
COPYGUARD_2:
  LD HL,GUARD4BUF         ; Point HL at the buffer for guard 1.
COPYGUARD_3:
  LD (GBUFADDR),HL        ; Save the address of the guard's buffer for later
                          ; retrieval.
  LD DE,GUARDLOC          ; Copy the guard's buffer into the temporary location
  LD BC,43                ; (GUARDLOC).
  LDIR                    ;
  RET

; Copy the temporary guard buffer back into place
;
; Used by the routines at START, MVSPRITES, REDRAW, DRAWGUARDS, CHKHIT, DEAD
; and CHKLUNCH.
UPDTGUARD:
  LD HL,(GBUFADDR)        ; Retrieve the address of the guard's buffer.
  LD DE,GUARDLOC          ; Point DE at the temporary guard buffer.
  EX DE,HL                ; Switch pointers.
  LD BC,43                ; Copy the temporary guard buffer back into place.
  LDIR                    ;
  RET

; Make the title screen or game over sound effect
;
; Used by the routines at START (on the title screen) and DEAD (when a game is
; over).
TITLESOUND:
  LD HL,SOUND             ; Pick up the sound on/off indicator.
  LD C,16                 ; Initialise C for the loop that follows.
TITLESOUND_0:
  PUSH BC                 ; Save the loop counter (unnecessarily).
  LD B,C                  ; Set B and D equal to 8*C. This value determines the
  SLA B                   ; pitch.
  SLA B                   ;
  SLA B                   ;
  LD D,B                  ;
  LD E,10                 ; This value determines the duration.
TITLESOUND_1:
  LD B,D                  ; Produce a sound (if the sound on/off indicator is
  LD A,31                 ; on) with pitch and duration determined by D and E.
  AND (HL)                ;
  OUT (254),A             ;
TITLESOUND_2:
  DJNZ TITLESOUND_2       ;
  LD A,7                  ;
  OUT (254),A             ;
  LD B,D                  ;
TITLESOUND_3:
  DJNZ TITLESOUND_3       ;
  DEC E                   ;
  JR NZ,TITLESOUND_1      ;
  POP BC                  ; Restore the loop counter to C (unnecessarily).
  DEC C                   ; Finished yet?
  JR NZ,TITLESOUND_0      ; Jump back if not.
  RET

; Get the tunnel offset and bell, initial guard and entrance locations for the
; current maze
;
; Used by the routine at START.
;
; HL MAZE1DATA, MAZE2DATA, MAZE3DATA or MAZE4DATA
CPMAZEDATA:
  LD DE,CMTUNNEL          ; Copy the tunnel offset and bell, initial guard and
  LD BC,8                 ; entrance locations for the current maze to
  LDIR                    ; CMTUNNEL.
  RET

; Copyright © 1982 Beam Software...
;
; Used by the routine at START.
COPYRIGHT:
  DEFM " Copyright "
  DEFB 127                ; ©
  DEFM " 1982 Beam Software "
  DEFM "  PRESS ANY KEY TO START PLAY  "
  DEFB 255                ; End marker.

; DEMO MODE  PRESS ANY KEY TO PLAY
;
; Used by the routine at START.
DEMOMODE:
  DEFM "DEMO MODE  PRESS ANY KEY TO PLAY"
  DEFB 255                ; End marker.

; Title screen countdown timer
;
; Used by the routine at START. Decremented until it reaches zero, after which
; demo mode begins.
TITLECT:
  DEFW 0

; Active guard counter
;
; Initialised by the routine at START, and used by the routines at DECGTIMERS,
; RINGBELL and INITGCT. Supposed to hold the number of active guards minus one,
; but may not because of a bug.
GUARDCOUNT:
  DEFB 0

; Game mode indicator
;
; Used by the routines at START, READKEYS, DEAD and PRINTDIGIT. Holds 0 in demo
; mode, or 1 in game mode.
GAMEMODE:
  DEFB 0

; Wait for 26*BC+5 T states
;
; Used by the routine at RINGBELL to produce a delay in place of a bell sound.
;
; BC Delay parameter
DELAY:
  DEC BC                  ; Decrement the delay counter.
  LD A,B                  ; Is it zero yet?
  OR C                    ;
  JR NZ,DELAY             ; Jump back if not.
  RET

; Collect a pseudo-random number (from the ROM)
;
; Used by the routines at READKEYS, DEAD and DROPLUNCH.
;
; O:A Pseudo-random number
RANDOM:
  LD HL,(RANDADDR)        ; Pick up the address of the next pseudo-random
                          ; number.
  LD A,(HL)               ; Copy the number to A.
  PUSH AF                 ; Save the number briefly.
  INC HL                  ; Increment the address, rolling over from 8191 to 0.
  LD A,H                  ;
  AND 31                  ;
  LD H,A                  ;
  LD (RANDADDR),HL        ; Save the new address.
  POP AF                  ; Restore the pseudo-random number to A.
  RET

; Print the score or high score
;
; Used by the routines at START and CHKEATEN.
;
; DE Display file address
; HL Score or high score
PRINTSCORE:
  EX DE,HL                ; Switch the display file address to HL.
  CALL ATTRADDR           ; Set HL' to the corresponding attribute file address
                          ; (unnecessarily).
  EX DE,HL                ; Switch the display file address back to DE, and the
                          ; score or high score to HL.
  LD A,"0"                ; Initialise A.
  AND A                   ; Clear the carry flag, ready for trial subtraction.
  LD BC,10000             ; Compute and print the 10,000s digit.
  CALL PRINTDIGIT         ;
  LD BC,1000              ; Compute and print the 1,000s digit.
  CALL PRINTDIGIT         ;
  LD BC,100               ; Compute and print the 100s digit.
  CALL PRINTDIGIT         ;
  LD BC,10                ; Compute and print the 10s digit.
  CALL PRINTDIGIT         ;
  LD A,L                  ; Compute the ASCII code for the 1s digit.
  ADD A,"0"               ;
  CALL PRINTDIGIT_0       ; Print it.
  RET

; Compute and print a digit
;
; Used by the routine at PRINTSCORE.
;
;   A 48 (ASCII code for '0')
;   BC 10000, 1000, 100 or 10
;   DE Display file address
;   HL Number being printed
;   F Carry flag reset
; O:A 48 (ASCII code for '0')
; O:F Carry flag reset
PRINTDIGIT:
  INC A                   ; Compute in A the ASCII code for the digit.
  SBC HL,BC               ;
  JR NC,PRINTDIGIT        ;
  ADD HL,BC               ;
  DEC A                   ;
; This entry point is used by the routines at START, CHKEATEN, DEAD and
; PRINTSCORE with A holding the ASCII code of the character to print.
PRINTDIGIT_0:
  PUSH BC                 ; Save BC.
  PUSH AF                 ; Save the character code briefly.
  LD A,(GAMEMODE)         ; Pick up the game mode indicator.
  LD B,A                  ; Copy it to B.
  POP AF                  ; Restore the character code to A.
  BIT 0,B                 ; Set the zero flag if it's demo mode.
  POP BC                  ; Restore BC.
  RET Z                   ; Return if it's demo mode.
  EX DE,HL                ; Swap DE and HL for no apparent reason.
  PUSH BC                 ; Save BC.
  PUSH DE                 ; Save DE.
  EX DE,HL                ; Swap DE and HL back again.
  LD H,0                  ; Compute in HL the address of the graphic data for
  LD L,A                  ; the character in the ROM.
  ADD HL,HL               ;
  ADD HL,HL               ;
  ADD HL,HL               ;
  LD BC,15360             ;
  ADD HL,BC               ;
  EX DE,HL                ; Point DE at the character's graphic data, and set
                          ; HL to the display file address.
  LD C,56                 ; This is the attribute byte for the character (INK
                          ; 0: PAPER 7).
  CALL PRINTTILE          ; Print the character.
  POP DE                  ; Restore DE.
  POP BC                  ; Restore BC.
  EX DE,HL                ; Point DE back at the display file, and restore the
                          ; original value of HL.
  LD A,"0"                ; Reset A to the ASCII code for '0'.
  AND A                   ; Clear the carry flag. (This instruction is
                          ; redundant.)
  RET

; Draw the current maze
;
; Used by the routine at START.
;
; BC 768
; DE 16384
; HL Address of the maze layout data (MAZE2, MAZE1, MAZE3 or MAZE4)
DRAWMAZE:
  PUSH BC                 ; Save the tile counter.
  PUSH HL                 ; Save the maze layout data address.
  EX DE,HL                ; Transfer the display file address to HL.
  CALL ATTRADDR           ; Set HL' to the corresponding attribute file address
                          ; (always 22528).
  EX DE,HL                ; Transfer the display file address back to DE.
  EXX                     ; Exchange registers.
  POP DE                  ; Restore the maze layout data address to DE'.
  POP BC                  ; Restore the tile counter to BC'.
DRAWMAZE_0:
  LD A,(DE)               ; Pick up a tile identifier.
  INC DE                  ; Point DE' at the next tile identifier.
  CP 0                    ; Is the current tile blank?
  JR Z,DRAWMAZE_3         ; Jump if so.
  CP 2                    ; Is the current tile a flower?
  JR Z,DRAWMAZE_4         ; Jump if so.
  CP 3                    ; Is the current tile an arrow (entrance or exit)?
  JR Z,DRAWMAZE_1         ; Jump if so.
  CP 9                    ; Is the current tile a tunnel entrance?
  JR Z,DRAWMAZE_2         ; Jump if so.
  LD A,61                 ; Wall tile (INK 5).
  JR DRAWMAZE_5
DRAWMAZE_1:
  LD A,56                 ; Arrow tile (INK 0).
  JR DRAWMAZE_5
DRAWMAZE_2:
  LD A,63                 ; Tunnel entrance (INK 7).
  JR DRAWMAZE_5
DRAWMAZE_3:
  LD A,62                 ; Blank tile (INK 6).
  JR DRAWMAZE_5
DRAWMAZE_4:
  LD A,60                 ; Flower (INK 4).
DRAWMAZE_5:
  LD (HL),A               ; Set the attribute byte.
  INC HL                  ; Move HL' along the attribute file.
  DEC BC                  ; Decrement the tile counter.
  LD A,B                  ; Have we finished setting the attribute bytes yet?
  OR C                    ;
  JR NZ,DRAWMAZE_0        ; Jump back if not.
  EXX                     ; Exchange registers.
; The attribute bytes have been set. Time to draw the maze tiles.
DRAWMAZE_6:
  PUSH HL                 ; Save the maze layout pointer.
  LD A,(HL)               ; Pick up a tile identifier in A.
  LD H,0                  ; Point IX at the graphic data for the corresponding
  LD L,A                  ; tile (at MAZETILES+8*A).
  ADD HL,HL               ;
  ADD HL,HL               ;
  ADD HL,HL               ;
  LD IX,MAZETILES         ;
  EX DE,HL                ;
  ADD IX,DE               ;
  EX DE,HL                ;
  LD L,8                  ; Draw the tile.
DRAWMAZE_7:
  LD A,(IX+0)             ;
  LD (DE),A               ;
  INC D                   ;
  INC IX                  ;
  DEC L                   ;
  JR NZ,DRAWMAZE_7        ;
  INC E                   ; Point DE at the display file address for the next
  JR Z,DRAWMAZE_8         ; tile.
  LD A,D                  ;
  SUB 8                   ;
  LD D,A                  ;
DRAWMAZE_8:
  POP HL                  ; Restore the maze layout pointer to HL.
  INC HL                  ; Move along to the next maze tile identifier.
  DEC BC                  ; Decrement the tile counter.
  LD A,B                  ; Have we drawn all the tiles yet?
  OR C                    ;
  JR NZ,DRAWMAZE_6        ; Jump back if not.
  RET

; Draw a sprite
;
; Used by the routines at REDRAW, DRAWGUARD, DEAD, DRAWHORACE and DRAWBELL.
;
; C Attribute byte
; DE Address of the sprite graphic data
; HL Sprite location
; HL' Attribute file address for the sprite location
DRAWSPRITE:
  EX AF,AF'               ; Copy the attribute byte to A'.
  LD A,C                  ;
  EX AF,AF'               ;
  LD BC,16384             ; Set HL to the appropriate display file address.
  ADD HL,BC               ;
  EX AF,AF'               ; Copy the attribute byte from A' back to C.
  LD C,A                  ;
  EX AF,AF'               ;
  CALL PRINTTILE          ; Draw the top-left tile.
  CALL PRINTTILE          ; Draw the top-right tile.
  LD A,L                  ; Set HL to the display file address for the
  ADD A,30                ; bottom-left tile.
  LD L,A                  ;
  JR NC,DRAWSPRITE_0      ;
  LD A,H                  ;
  ADD A,8                 ;
  LD H,A                  ;
DRAWSPRITE_0:
  CALL ATTRADDR           ; Set HL' to the corresponding attribute file
                          ; address.
  CALL PRINTTILE          ; Draw the bottom-left tile.
  CALL PRINTTILE          ; Draw the bottom-right tile.
  RET

; Print a tile or font character
;
; Used by the routines at REDRAWG, PRINTDIGIT and DRAWSPRITE.
;
;   C Attribute byte
;   DE Address of the sprite tile or font character graphic data
;   HL Display file address
;   HL' Attribute file address
; O:HL Display file address for the next tile to the right
; O:HL' Attribute file address for the next tile to the right
PRINTTILE:
  LD A,C                  ; Copy the attribute byte to A.
  EXX                     ; Exchange registers.
  LD (HL),A               ; Set the attribute byte.
  INC HL                  ; Point HL' at the next attribute byte.
  EXX                     ; Exchange registers.
  LD B,8                  ; Copy the 8 graphic bytes of the tile to the screen.
PRINTTILE_0:
  LD A,(DE)               ;
  LD (HL),A               ;
  INC H                   ;
  INC DE                  ;
  DJNZ PRINTTILE_0        ;
  INC L                   ; Set HL to the display file address for the next
  RET Z                   ; tile to the right.
  LD B,8                  ;
  LD A,H                  ;
  SUB B                   ;
  LD H,A                  ;
  RET

; Convert a sprite location or display file address into an attribute file
; address
;
; Used by the routines at START, REDRAW, REDRAWG, CHKEATEN, DRAWGUARD, DEAD,
; DRAWHORACE, DRAWBELL, PRINTSCORE, DRAWMAZE, DRAWSPRITE, CHKATTRS, CPMAZEBG
; and CHKTILES.
;
;   HL Sprite location or display file address
; O:HL' Attribute file address
ATTRADDR:
  PUSH HL                 ; Push the address/location onto the stack.
  EXX                     ; Exchange registers.
  POP HL                  ; Drop the address/location off the stack into HL'.
  LD A,H                  ; Set HL' to the corresponding attribute file
  AND 24                  ; address.
  SRA A                   ;
  SRA A                   ;
  SRA A                   ;
  ADD A,88                ;
  LD H,A                  ;
  EXX                     ; Exchange registers.
  RET

; Check the attribute bytes at a sprite's location
;
; Used by the routines at CHKEATEN and CHKHIT.
;
;   HL Sprite location
; O:A 58 (cherry/strawberry), 59 (guard), 60 (flower), or 0 (none of these)
; O:F Zero flag set if A is 0 (nothing found)
CHKATTRS:
  PUSH BC                 ; Save BC.
  PUSH DE                 ; Save DE.
  PUSH HL                 ; Save the sprite location.
  LD BC,16384             ; Convert the sprite location into a display file
  ADD HL,BC               ; address.
  CALL ATTRADDR           ; Set HL' to the corresponding attribute file
                          ; address.
  EXX                     ; Exchange registers.
  LD E,0                  ; E' will hold the attribute of any interesting tile
                          ; at this location; initialise it now.
  CALL CHKATTR            ; Check the attribute of the top-left tile.
  INC HL                  ; Point HL' at the top-right tile.
  CALL CHKATTR            ; Check the attribute of the top-right tile.
  LD BC,31                ; Point HL' at the bottom-left tile.
  ADD HL,BC               ;
  CALL CHKATTR            ; Check the attribute of the bottom-left tile.
  INC HL                  ; Point HL' at the bottom-right tile.
  CALL CHKATTR            ; Check the attribute of the bottom-right tile.
  LD A,E                  ; Copy the tile attribute indicator to A.
  EXX                     ; Exchange registers.
  POP HL                  ; Restore the sprite location to HL.
  POP DE                  ; Restore DE.
  POP BC                  ; Restore BC.
  AND A                   ; Set the zero flag if no interesting tile (guard,
                          ; flower, cherry, strawberry) was found.
  RET

; Check the attribute byte at a sprite tile location
;
; Used by the routine at CHKATTRS.
;
;   E 0, or attribute value from previous call
;   HL Attribute file address
; O:E 58 (cherry/strawberry), 59 (guard), 60 (flower), or 0 (none of these)
CHKATTR:
  LD A,(HL)               ; Pick up the attribute byte.
  CP 59                   ; Is it magenta?
  JR Z,CHKATTR_0          ; Jump if so (there is a guard here).
  CP 58                   ; Is it red?
  JR Z,CHKATTR_1          ; Jump if so (there is a cherry or strawberry here).
  CP 60                   ; Is it green?
  JR Z,CHKATTR_2          ; Jump if so (there is a flower here).
  JR CHKATTR_3
CHKATTR_0:
  LD E,A                  ; E=59 (guard).
  JR CHKATTR_3
CHKATTR_1:
  LD D,A                  ; D=58 (cherry or strawberry).
  LD A,E                  ; Have we already detected a guard at the sprite's
  CP 59                   ; location?
  JR Z,CHKATTR_3          ; Jump if so (it doesn't matter that there's also a
                          ; cherry or strawberry here).
  LD E,D                  ; E=58 (cherry or strawberry).
  JR CHKATTR_3
CHKATTR_2:
  LD D,A                  ; D=60 (flower).
  LD A,E                  ; Have we already detected a guard, cherry,
  AND A                   ; strawberry or flower at the sprite's location?
  JR NZ,CHKATTR_3         ; Jump if so.
  LD E,D                  ; E=60 (flower).
CHKATTR_3:
  RET

; Copy maze background tiles into a guard's buffer
;
; Used by the routines at REDRAWG and CHKHIT.
;
; HL Guard's new location
CPMAZEBG:
  PUSH BC                 ; Save BC.
  PUSH DE                 ; Save DE.
  PUSH HL                 ; Save the guard's location.
  LD BC,16384             ; Convert the guard's location into a display file
  ADD HL,BC               ; address.
  CALL ATTRADDR           ; Set HL' to the corresponding attribute file
                          ; address.
  LD DE,GUARDBG           ; Point DE at the first of the maze background tiles
                          ; in the temporary guard buffer.
  CALL CPMAZETILE         ; Copy the top-left maze background tile into the
                          ; temporary guard buffer.
  CALL CPMAZETILE         ; Copy the top-right maze background tile into the
                          ; temporary guard buffer.
  EXX                     ; Exchange registers.
  LD BC,30                ; Point HL' at the bottom-left tile's attribute byte.
  ADD HL,BC               ;
  EXX                     ; Exchange registers.
  LD A,L                  ; Point HL at the bottom-left tile in the display
  ADD A,30                ; file.
  LD L,A                  ;
  JR NC,CPMAZEBG_0        ;
  LD A,H                  ;
  ADD A,8                 ;
  LD H,A                  ;
CPMAZEBG_0:
  CALL CPMAZETILE         ; Copy the bottom-left maze background tile into the
                          ; temporary guard buffer.
  CALL CPMAZETILE         ; Copy the bottom-right maze background tile into the
                          ; temporary guard buffer.
  POP DE                  ; Restore the guard's location to DE.
  POP HL                  ; Restore the value in DE on entry to HL.
  POP BC                  ; Restore BC.
  RET

; Copy a maze background tile into a guard's buffer
;
; Used by the routine at CPMAZEBG.
;
;   DE Address of the tile in the guard's buffer
;   HL Display file address of the maze background tile
;   HL' Attribute file address of the maze background tile
; O:HL Display file address of the next maze background tile to the right
; O:HL' Attribute file address of the next maze background tile to the right
CPMAZETILE:
  EXX                     ; Exchange registers.
  LD A,(HL)               ; Pick up the attribute byte of the maze background
                          ; tile.
  INC HL                  ; Point HL' at the attribute byte of the next tile to
                          ; the right.
  EXX                     ; Exchange registers.
  CP 59                   ; Is there a magenta (guard sprite) tile here?
  JR Z,CPMAZETILE_2       ; Jump if so.
  LD (DE),A               ; Copy the maze background tile attribute byte into
                          ; the temporary guard buffer.
  INC DE                  ; Point DE at the first graphic byte of the tile in
                          ; the temporary guard buffer.
  LD B,8                  ; Copy the maze background tile's graphic bytes into
CPMAZETILE_0:
  LD A,(HL)               ; the temporary guard buffer.
  LD (DE),A               ;
  INC DE                  ;
  INC H                   ;
  DJNZ CPMAZETILE_0       ;
  INC L                   ; Point HL at the first graphic byte of the next maze
  JR Z,CPMAZETILE_1       ; background tile to the right.
  LD A,H                  ;
  SUB 8                   ;
  LD H,A                  ;
CPMAZETILE_1:
  RET
; There is a guard sprite tile at this location.
CPMAZETILE_2:
  LD A,62                 ; Set the background tile attribute byte (INK 6:
  LD (DE),A               ; PAPER 7).
  INC DE                  ; Point DE at the first graphic byte of the tile in
                          ; the temporary guard buffer.
  LD B,8                  ; Clear out the background tile in the temporary
  LD A,0                  ; guard buffer. Doing this means that, in some
CPMAZETILE_3:
  LD (DE),A               ; situations, the guard will eat the flowers or fruit
  INC DE                  ; in his path.
  DJNZ CPMAZETILE_3       ;
  INC L                   ; Point HL at the first graphic byte of the next maze
  RET NZ                  ; background tile to the right.
  LD A,H                  ;
  ADD A,8                 ;
  LD H,A                  ;
  RET

; Check the tiles next to Horace or a guard
;
; Used by the routines at READKEYS, MVSPRITES and MVGUARD.
;
;   A Sprite animation frame
;   HL Sprite location
; O:A 1 (tunnel entrance), 2 (maze exit/entrance), 3 (wall), or 0 (none of
;     these)
CHKTILES:
  PUSH BC                 ; Save BC.
  PUSH DE                 ; Save DE.
  PUSH HL                 ; Save the sprite location.
  LD BC,16384             ; Convert the sprite location into a display file
  ADD HL,BC               ; address.
  CALL INFRONT            ; Set HL the display file address of the tile in
                          ; front of the sprite.
  CALL ATTRADDR           ; Set HL' to the corresponding attribute file
                          ; address.
  EXX                     ; Exchange registers.
  LD E,0                  ; E' will hold the indicator of any interesting tiles
                          ; at this location; initialise it now.
  CALL IDTILE             ; Check the top-left tile.
  INC HL                  ; Point HL' at the top-right tile.
  CALL IDTILE             ; Check the top-right tile.
  LD BC,31                ; Point HL' at the bottom-left tile.
  ADD HL,BC               ;
  CALL IDTILE             ; Check the bottom-left tile.
  INC HL                  ; Point HL' at the bottom-right tile.
  CALL IDTILE             ; Check the bottom-right tile.
  LD A,E                  ; Copy the tile indicator to A.
  EXX                     ; Exchange registers.
  POP HL                  ; Restore the sprite location to HL.
  POP DE                  ; Restore DE.
  POP BC                  ; Restore BC.
  CP 255                  ; Is the sprite facing a wall tile?
  JR NZ,CHKTILES_0        ; Jump if not.
  LD A,3                  ; Signal: sprite is facing a wall tile.
  RET
CHKTILES_0:
  AND 10                  ; Return with A=1 if the sprite is facing a tunnel
  RRC A                   ; entrance.
  CP 1                    ;
  RET Z                   ;
  RRC A                   ; Now A=2 if the sprite is facing the maze
                          ; exit/entrance, or 0 otherwise.
  RET

; Identify a tile in front of a sprite
;
; Used by the routine at CHKTILES.
;
;   E Previous tile indicator
;   HL Attribute file address for the tile
; O:E Updated tile indicator
IDTILE:
  LD A,E                  ; Copy the previous tile indicator to A.
  CP 255                  ; Have we already found a wall tile?
  RET Z                   ; Return if so.
  LD A,(HL)               ; Pick up the tile's attribute byte.
  CP 61                   ; Is it a wall tile?
  JR NZ,IDTILE_0          ; Jump if not.
  LD E,255                ; Signal: wall tile found.
  RET
IDTILE_0:
  CP 63                   ; Is it a tunnel entrance?
  JR NZ,IDTILE_1          ; Jump if not.
  LD A,1                  ; Record the number of tunnel entrance tiles found so
  ADD A,E                 ; far in bits 0 and 1 of E.
  LD E,A                  ;
  RET
IDTILE_1:
  CP 56                   ; Is it an arrow tile (maze entrance/exit)?
  RET NZ                  ; Return if not.
  LD A,4                  ; Record the number of arrow tiles found so far in
  ADD A,E                 ; bits 2 and 3 of E.
  LD E,A                  ;
  RET

; Get the location of the tile in front of a sprite
;
; Used by the routines at MVSPRITES, MVGUARD and CHKTILES.
;
;   A Sprite animation frame
;   HL Sprite location
; O:HL Location of the tile in front of the sprite
INFRONT:
  AND 3                   ; Keep only bits 0 and 1 of the animation frame
                          ; (which indicate the direction that the sprite is
                          ; facing). (This instruction is redundant.)
  CP 0                    ; Is the sprite facing up?
  JR Z,INFRONT_0          ; Jump if so.
  CP 1                    ; Is the sprite facing right?
  JR Z,INFRONT_1          ; Jump if so.
  CP 2                    ; Is the sprite facing down?
  JR Z,INFRONT_2          ; Jump if so.
; The sprite is facing left.
  DEC L                   ; Point HL at the tile to the left of the sprite's
                          ; current location.
  JR NC,INFRONT_3         ; This jump is always made.
  LD A,H                  ; This code is never executed.
  SUB 8                   ;
  LD H,A                  ;
  JR INFRONT_3            ;
; The sprite is facing up.
INFRONT_0:
  LD A,L                  ; Point HL at the tile above the sprite's current
  SUB 32                  ; location.
  LD L,A                  ;
  JR NC,INFRONT_3         ;
  LD A,H                  ;
  SUB 8                   ;
  LD H,A                  ;
  JR INFRONT_3
; The sprite is facing right.
INFRONT_1:
  INC L                   ; Point HL at the tile to the right of the sprite's
  JR NZ,INFRONT_3         ; current location.
  LD A,H                  ;
  ADD A,8                 ;
  LD H,A                  ;
  JR INFRONT_3
; The sprite is facing down.
INFRONT_2:
  LD A,L                  ; Point HL at the tile below the sprite's current
  ADD A,32                ; location.
  LD L,A                  ;
  JR NC,INFRONT_3         ;
  LD A,H                  ;
  ADD A,8                 ;
  LD H,A                  ;
INFRONT_3:
  RET

; Print 'HUNGRY' or 'HORACE' on the title screen
;
; Used by the routine at START.
;
; BC 160 (number of tiles to draw)
; DE Display file address
; HL HUNGRY (HUNGRY) or HORACE (HORACE)
BIGWORD:
  PUSH BC                 ; Save the tile counter.
  LD A,(HL)               ; Pick up a data byte.
  AND 48                  ; Keep only bits 4 and 5.
  CP 0                    ; Are bits 4 and 5 both reset?
  JR Z,BIGWORD_2          ; Jump if so.
  CP 16                   ; Is bit 4 set and bit 5 reset?
  JR Z,BIGWORD_1          ; Jump if so.
  CP 32                   ; Is bit 4 reset and bit 5 set?
  JR Z,BIGWORD_0          ; Jump if so.
  LD C,%11111111          ; Bits 5 and 4 of A are both set.
  JR BIGWORD_3
BIGWORD_0:
  LD C,%00001111          ; Bit 5 of A is reset and bit 4 is set.
  JR BIGWORD_3
BIGWORD_1:
  LD C,%11110000          ; Bit 5 of A is set and bit 4 is reset.
  JR BIGWORD_3
BIGWORD_2:
  LD C,%00000000          ; Bits 5 and 4 of A are both reset.
BIGWORD_3:
  LD B,4                  ; Draw the top four pixel rows of the character cell
  LD A,C                  ; using the bit pattern in C.
BIGWORD_4:
  LD (DE),A               ;
  INC D                   ;
  DJNZ BIGWORD_4          ;
  LD A,(HL)               ; Pick up the data byte again.
  AND 3                   ; Keep only bits 0 and 1.
  CP 0                    ; Are bits 0 and 1 both reset?
  JR Z,BIGWORD_7          ; Jump if so.
  CP 1                    ; Is bit 0 set and bit 1 reset?
  JR Z,BIGWORD_6          ; Jump if so.
  CP 2                    ; Is bit 0 reset and bit 1 set?
  JR Z,BIGWORD_5          ; Jump if so.
  LD C,%11111111          ; Bits 1 and 0 of A are both set.
  JR BIGWORD_8
BIGWORD_5:
  LD C,%00001111          ; Bit 1 of A is reset and bit 0 is set.
  JR BIGWORD_8
BIGWORD_6:
  LD C,%11110000          ; Bit 1 of A is set and bit 0 is reset.
  JR BIGWORD_8
BIGWORD_7:
  LD C,%00000000          ; Bits 1 and 0 of A are both reset.
BIGWORD_8:
  LD B,4                  ; Draw the bottom four pixel rows of the character
  LD A,C                  ; cell using the bit pattern in C.
BIGWORD_9:
  LD (DE),A               ;
  INC D                   ;
  DJNZ BIGWORD_9          ;
  INC E                   ; Set DE to the display file address of the next cell
  JR Z,BIGWORD_10         ; to the right.
  LD A,D                  ;
  SUB 8                   ;
  LD D,A                  ;
BIGWORD_10:
  INC HL                  ; Point HL at the next data byte.
  POP BC                  ; Restore the tile counter to BC.
  DEC BC                  ; Decrement the tile counter.
  LD A,B                  ; Is it zero now?
  OR C                    ;
  JR NZ,BIGWORD           ; Jump back if not.
  RET

; Guard countdown timers
;
; Initialised by the routine at INITGCT, and used by the routines at MVSPRITES,
; REDRAW, DRAWGUARDS, CHKHIT, DEAD, CHKLUNCH and DECGTIMERS. When a guard's
; countdown timer reaches 0, the guard is brought into play.
GUARD1CT:
  DEFW 0                  ; Guard 1.
GUARD2CT:
  DEFW 0                  ; Guard 2.
GUARD3CT:
  DEFW 0                  ; Guard 3.
GUARD4CT:
  DEFW 0                  ; Guard 4.

; Guard panic timer
;
; Initialised by the routine at START, and used by the routines at MVGUARD,
; DRAWGUARD, CHKHIT and DRAWBELL. Holds a non-zero value when the guards are
; panicking.
GUARDPT:
  DEFW 0

; Temporary store for a guard's screen x- and y-coordinates
;
; Used by the routine at MVGUARD.
TEMPXY:
  DEFB 0                  ; x-coordinate.
  DEFB 0                  ; y-coordinate.

; Temporary store for direction probability parameters
;
; Used by the routine at READKEYS (when moving Horace in demo mode) and MVGUARD
; (when moving a guard).
TEMPDPP:
  DEFB 0                  ; Up.
  DEFB 0                  ; Right.
  DEFB 0                  ; Down.
  DEFB 0                  ; Left.

; Buffer address of the guard currently being handled
;
; Used by the routines at COPYGUARD and UPDTGUARD.
GBUFADDR:
  DEFW 0

; Temporary guard buffer
;
; The contents of a guard's buffer (see GUARD1BUF) are copied here by the
; routine at COPYGUARD whenever the guard needs to be moved, drawn or otherwise
; examined. After that, the updated contents are copied back to the original
; location by the routine at UPDTGUARD.
GUARDLOC:
  DEFW 0                  ; Current location. Used by the routines at START,
                          ; MVGUARD, REDRAWG, DRAWGUARD, CHKHIT, COPYGUARD and
                          ; UPDTGUARD.
GUARDAF:
  DEFB 0                  ; Animation frame. Used by the routines at MVGUARD,
                          ; DRAWGUARD, CHKHIT and DRAWBELL.
GUARDTIMER:
  DEFB 0                  ; Animation frame timer. Used by the routine at
                          ; DRAWGUARD.
GUARDNLOC:
  DEFW 0                  ; New location. Used by the routines at START,
                          ; MVGUARD, REDRAWG, DRAWGUARD and CHKHIT.
GUARDDELAY:
  DEFB 0                  ; Return delay counter. Used by the routines at
                          ; MVGUARD, DRAWGUARD, CHKHIT, DEAD and DROPLUNCH.
GUARDBG:
  DEFB 0,0,0,0,0,0,0,0,0  ; Maze background tiles at the guard's location. Used
  DEFB 0,0,0,0,0,0,0,0,0  ; by the routines at START, REDRAWG, DROPLUNCH and
  DEFB 0,0,0,0,0,0,0,0,0  ; CPMAZEBG.
  DEFB 0,0,0,0,0,0,0,0,0  ;

; Guard buffers
;
; Used by the routines at COPYGUARD and UPDTGUARD. See GUARDLOC for a
; description of the contents of a guard's buffer.
GUARD1BUF:
  DEFW 0                  ; Guard 1.
  DEFB 0                  ;
  DEFB 0                  ;
  DEFW 0                  ;
  DEFB 0                  ;
  DEFB 0,0,0,0,0,0,0,0,0  ;
  DEFB 0,0,0,0,0,0,0,0,0  ;
  DEFB 0,0,0,0,0,0,0,0,0  ;
  DEFB 0,0,0,0,0,0,0,0,0  ;
GUARD2BUF:
  DEFW 0                  ; Guard 2.
  DEFB 0                  ;
  DEFB 0                  ;
  DEFW 0                  ;
  DEFB 0                  ;
  DEFB 0,0,0,0,0,0,0,0,0  ;
  DEFB 0,0,0,0,0,0,0,0,0  ;
  DEFB 0,0,0,0,0,0,0,0,0  ;
  DEFB 0,0,0,0,0,0,0,0,0  ;
GUARD3BUF:
  DEFW 0                  ; Guard 3.
  DEFB 0                  ;
  DEFB 0                  ;
  DEFW 0                  ;
  DEFB 0                  ;
  DEFB 0,0,0,0,0,0,0,0,0  ;
  DEFB 0,0,0,0,0,0,0,0,0  ;
  DEFB 0,0,0,0,0,0,0,0,0  ;
  DEFB 0,0,0,0,0,0,0,0,0  ;
GUARD4BUF:
  DEFW 0                  ; Guard 4.
  DEFB 0                  ;
  DEFB 0                  ;
  DEFW 0                  ;
  DEFB 0                  ;
  DEFB 0,0,0,0,0,0,0,0,0  ;
  DEFB 0,0,0,0,0,0,0,0,0  ;
  DEFB 0,0,0,0,0,0,0,0,0  ;
  DEFB 0,0,0,0,0,0,0,0,0  ;

; Data for the word 'HUNGRY' on the title screen
;
; Used by the routine at BIGWORD.
;
; Each byte here determines the bit pattern that is drawn in a single tile.
; Bits 4 and 5 determine the pattern in the upper half of the tile, and bits 0
; and 1 determine the pattern in the lower half.
HUNGRY:
  DEFB 51,17,0,51,17,34,51,0 ; First row of tiles.
  DEFB 34,51,0,51,17,0,51,17 ;
  DEFB 0,35,49,51,1,0,51,49  ;
  DEFB 50,19,0,34,51,0,34,51 ;
  DEFB 51,17,0,51,17,34,51,0 ; Second row of tiles.
  DEFB 34,51,0,51,51,1,51,17 ;
  DEFB 34,51,0,32,48,0,51,17 ;
  DEFB 0,51,17,34,51,0,34,51 ;
  DEFB 51,19,3,51,17,34,51,0  ; Third row of tiles.
  DEFB 34,51,0,51,49,51,51,17 ;
  DEFB 34,51,2,3,3,0,51,19    ;
  DEFB 35,49,0,0,50,19,51,16  ;
  DEFB 51,17,0,51,17,34,51,0  ; Fourth row of tiles.
  DEFB 34,51,0,51,17,32,51,17 ;
  DEFB 34,51,0,34,51,0,51,49  ;
  DEFB 51,1,0,0,0,51,17,0     ;
  DEFB 51,17,0,51,17,0,50,19 ; Fifth row of tiles.
  DEFB 51,16,0,51,17,0,51,17 ;
  DEFB 0,50,19,51,16,0,51,17 ;
  DEFB 32,51,1,0,0,51,17,0   ;

; Data for the word 'HORACE' on the title screen
;
; Used by the routine at BIGWORD.
;
; Each byte here determines the bit pattern that is drawn in a single tile.
; Bits 4 and 5 determine the pattern in the upper half of the tile, and bits 0
; and 1 determine the pattern in the lower half.
HORACE:
  DEFB 51,17,0,51,17,0,35,49  ; First row of tiles.
  DEFB 51,1,0,51,49,50,19,0   ;
  DEFB 0,35,49,51,1,0,2,51    ;
  DEFB 50,19,0,34,51,48,48,16 ;
  DEFB 51,17,0,51,17,34,51,0 ; Second row of tiles.
  DEFB 34,51,0,51,17,0,51,17 ;
  DEFB 34,51,0,34,51,0,51,17 ;
  DEFB 0,48,16,34,51,0,0,0   ;
  DEFB 51,19,3,51,17,34,51,0 ; Third row of tiles.
  DEFB 34,51,0,51,19,35,49,0 ;
  DEFB 34,51,3,35,51,0,51,17 ;
  DEFB 0,0,0,34,51,3,1,0     ;
  DEFB 51,17,0,51,17,34,51,0 ; Fourth row of tiles.
  DEFB 34,51,0,51,49,51,1,0  ;
  DEFB 34,51,0,34,51,0,51,17 ;
  DEFB 0,3,1,34,51,0,0,0     ;
  DEFB 51,17,0,51,17,0,50,19 ; Fifth row of tiles.
  DEFB 51,16,0,51,17,32,51,1 ;
  DEFB 34,51,0,34,51,0,32,51 ;
  DEFB 35,49,0,34,51,3,3,1   ;

; Tunnel offset and bell, initial guard and entrance locations for the current
; maze
;
; Used by the routines at START, MVSPRITES, MVGUARD, CHKHIT, DRAWBELL and
; CPMAZEDATA. The values here are copied from MAZE1DATA, MAZE2DATA, MAZE3DATA
; or MAZE4DATA.
CMTUNNEL:
  DEFW 0                  ; Tunnel offset.
CMBELLLOC:
  DEFW 0                  ; Bell location.
CMINITGL:
  DEFW 0                  ; Initial guard location.
CMENTRANCE:
  DEFW 0                  ; Entrance location.

; Tunnel offset and bell, initial guard and entrance locations for maze 1
;
; Used by the routine at START.
MAZE1DATA:
  DEFW 63488              ; Tunnel offset (up 8 tiles).
  DEFW 4202               ; Bell location: (x,y)=(10,19).
  DEFW 2197               ; Initial guard location: (x,y)=(21,12).
  DEFW 2209               ; Entrance location: (x,y)=(1,13).

; Tunnel offset and bell, initial guard and entrance locations for maze 2
;
; Used by the routine at START.
MAZE2DATA:
  DEFW 4192               ; Tunnel offset (down 19 tiles).
  DEFW 252                ; Bell location: (x,y)=(28,7).
  DEFW 4112               ; Initial guard location: (x,y)=(16,16).
  DEFW 2113               ; Entrance location: (x,y)=(1,10).

; Tunnel offset and bell, initial guard and entrance locations for maze 3
;
; Used by the routine at START.
MAZE3DATA:
  DEFW 0                  ; Tunnel offset (no tunnel in this maze).
  DEFW 189                ; Bell location: (x,y)=(29,5).
  DEFW 2092               ; Initial guard location: (x,y)=(12,9).
  DEFW 4257               ; Entrance location: (x,y)=(1,21).

; Tunnel offset and bell, initial guard and entrance locations for maze 4
;
; Used by the routine at START.
MAZE4DATA:
  DEFW 4064               ; Tunnel offset (down 15 tiles).
  DEFW 125                ; Bell location: (x,y)=(29,3).
  DEFW 2159               ; Initial guard location: (x,y)=(15,11).
  DEFW 97                 ; Entrance location: (x,y)=(1,3).

; Maze 2 layout
;
; Used by the routine at DRAWMAZE.
MAZE2:
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,4,9,9,4
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,4,0,0,4
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 5,1,1,1,1,1,1,1,1,1,1,1,4,0,0,4
  DEFB 1,1,1,1,1,1,1,1,1,1,1,1,1,1,6,0
  DEFB 4,0,2,0,0,2,0,0,2,0,0,2,4,0,0,4
  DEFB 0,2,0,0,2,0,0,2,0,0,2,0,0,2,4,0
  DEFB 4,0,0,0,0,0,0,0,0,0,0,0,4,0,0,4
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,0
  DEFB 4,0,0,5,1,1,1,1,1,1,0,0,4,0,0,4
  DEFB 0,0,4,0,0,4,0,0,1,1,1,1,0,0,4,0
  DEFB 4,0,2,4,0,2,0,0,2,0,0,2,0,0,2,0
  DEFB 0,2,4,0,2,4,0,2,0,0,2,0,0,0,4,0
  DEFB 4,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,4,0,0,4,0,0,0,0,0,0,0,0,4,0
  DEFB 4,0,0,4,0,0,5,1,1,1,1,1,1,1,1,1
  DEFB 0,0,4,0,0,7,1,1,1,1,1,1,1,1,8,0
  DEFB 3,0,0,4,0,2,4,0,2,0,0,2,0,0,2,0
  DEFB 0,2,4,0,2,0,0,2,0,0,2,0,0,0,3,0
  DEFB 3,0,0,4,0,0,4,0,0,0,0,0,0,0,0,0
  DEFB 0,0,4,0,0,0,0,0,0,0,0,0,0,0,3,0
  DEFB 4,0,0,4,0,0,4,0,0,5,1,1,1,1,1,1
  DEFB 0,0,4,0,0,1,1,1,6,0,0,4,0,0,4,0
  DEFB 4,0,2,4,0,2,4,0,2,4,0,2,0,0,2,0
  DEFB 0,2,4,0,2,0,0,2,4,0,2,4,0,2,4,0
  DEFB 4,0,0,4,0,0,4,0,0,4,0,0,0,0,0,0
  DEFB 0,0,4,0,0,0,0,0,4,0,0,4,0,0,4,0
  DEFB 4,0,0,4,0,0,4,0,0,4,0,0,5,1,1,1
  DEFB 0,0,4,0,0,4,0,0,4,0,0,4,0,0,4,0
  DEFB 4,0,2,4,0,2,0,0,2,4,0,2,4,0,2,0
  DEFB 0,2,0,0,2,4,0,2,0,0,2,4,0,2,4,0
  DEFB 4,0,0,4,0,0,0,0,0,4,0,0,4,0,0,0
  DEFB 0,0,0,0,0,4,0,0,0,0,0,4,0,0,4,0
  DEFB 4,0,0,7,1,1,1,1,1,8,0,0,4,0,0,4
  DEFB 0,0,1,1,1,1,1,1,1,1,1,8,0,0,4,0
  DEFB 4,0,2,0,0,2,0,0,2,0,0,2,4,0,0,4
  DEFB 0,2,0,0,2,0,0,2,0,0,2,0,0,2,4,0
  DEFB 4,0,0,0,0,0,0,0,0,0,0,0,4,0,0,4
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,0
  DEFB 7,1,1,1,1,1,1,1,1,1,1,1,4,0,0,4
  DEFB 1,1,1,1,1,1,1,1,1,1,1,1,1,1,8,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,4,0,0,4
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,4,9,9,4
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0

; Maze 1 layout
;
; Used by the routine at DRAWMAZE.
MAZE1:
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 5,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
  DEFB 1,1,1,1,1,1,1,1,1,1,1,1,1,1,6,0
  DEFB 4,0,2,0,0,2,0,0,2,4,0,2,0,0,2,0
  DEFB 0,2,0,0,2,4,0,2,0,0,2,0,0,2,4,0
  DEFB 4,0,0,0,0,0,0,0,0,4,0,0,0,0,0,0
  DEFB 0,0,0,0,0,4,0,0,0,0,0,0,0,0,4,0
  DEFB 4,0,0,5,1,1,1,0,0,4,0,0,1,1,1,1
  DEFB 0,0,0,0,0,4,0,0,1,1,1,6,0,0,4,0
  DEFB 4,0,2,4,0,2,0,0,2,0,0,2,0,0,2,0
  DEFB 0,2,0,0,2,0,0,2,0,0,2,4,0,2,4,0
  DEFB 4,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,4,0,0,4,0
  DEFB 4,0,0,4,0,0,5,1,1,1,1,1,1,1,1,6
  DEFB 0,0,5,1,1,1,1,1,6,0,0,4,0,0,4,0
  DEFB 4,0,2,4,0,2,4,0,0,0,0,0,5,1,1,1
  DEFB 1,1,1,1,1,6,0,0,4,0,2,0,0,2,4,0
  DEFB 4,0,0,4,0,0,4,0,0,5,1,1,8,0,0,0
  DEFB 0,0,0,0,0,7,1,1,4,0,0,0,0,0,4,0
  DEFB 4,0,0,4,0,0,7,1,1,8,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,7,1,1,1,0,0,4,0
  DEFB 3,0,0,4,0,2,0,0,0,0,0,0,0,0,0,5
  DEFB 1,1,6,0,0,0,0,0,0,0,0,0,0,2,4,0
  DEFB 3,0,0,4,0,0,0,0,0,0,0,0,5,1,1,8
  DEFB 9,9,7,1,1,6,0,0,0,0,0,0,0,0,4,0
  DEFB 4,0,0,4,0,0,5,1,1,1,1,1,8,0,0,0
  DEFB 0,0,4,0,0,7,1,1,1,1,1,1,0,0,4,0
  DEFB 4,0,2,4,0,2,4,0,2,0,0,2,0,0,2,0
  DEFB 0,2,4,0,2,0,0,2,0,0,2,0,0,2,4,0
  DEFB 4,0,0,4,0,0,4,0,0,0,0,0,0,0,0,0
  DEFB 0,0,4,0,0,0,0,0,0,0,0,0,0,0,4,0
  DEFB 4,0,0,4,0,0,4,0,0,5,1,1,1,1,1,1
  DEFB 1,1,8,0,0,4,0,0,1,1,1,1,0,0,4,0
  DEFB 4,0,2,0,0,2,0,0,2,4,0,0,0,0,2,0
  DEFB 0,2,0,0,2,4,0,2,0,0,2,0,0,0,3,0
  DEFB 4,0,0,0,0,0,0,0,0,4,0,0,0,0,0,0
  DEFB 0,0,0,0,0,4,0,0,0,0,0,0,0,0,3,0
  DEFB 7,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
  DEFB 1,1,1,1,1,1,1,1,1,1,1,1,1,1,8,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0

; Maze 3 layout
;
; Used by the routine at DRAWMAZE.
MAZE3:
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,5,1,1,1,1,1,1,1,1,1
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5
  DEFB 1,1,1,1,1,1,8,0,0,0,0,0,0,0,0,3
  DEFB 0,0,0,0,0,0,0,0,5,1,1,1,1,1,1,8
  DEFB 0,2,0,0,2,0,0,0,2,0,0,0,0,0,0,3
  DEFB 5,1,1,1,1,1,1,1,8,0,0,2,0,0,2,0
  DEFB 0,0,0,0,0,0,0,0,0,5,1,1,1,1,1,6
  DEFB 4,0,2,0,0,2,0,0,2,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,5,1,1,1,8,0,0,4,0,0,4
  DEFB 4,0,0,0,0,0,0,0,0,0,0,5,1,1,1,1
  DEFB 1,1,6,0,2,7,1,1,1,1,1,6,4,0,0,4
  DEFB 4,0,0,1,1,1,1,1,1,1,1,4,0,0,0,0
  DEFB 0,0,4,0,0,0,0,0,2,0,0,7,8,0,0,4
  DEFB 4,0,2,0,0,2,0,0,2,0,0,7,1,1,1,1
  DEFB 1,1,4,0,0,0,0,0,0,0,0,2,0,0,0,4
  DEFB 4,0,0,0,0,0,0,0,0,0,0,2,0,0,2,0
  DEFB 0,0,7,1,1,1,1,1,6,0,0,0,0,0,0,4
  DEFB 7,1,1,1,1,1,1,1,6,0,0,0,0,0,0,0
  DEFB 0,2,0,0,2,0,0,0,7,1,1,1,1,1,1,4
  DEFB 0,0,0,0,0,0,0,0,7,1,1,1,1,1,1,6
  DEFB 0,0,0,0,0,0,0,0,2,0,0,2,0,0,2,4
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7
  DEFB 1,1,1,1,1,6,0,0,0,0,0,0,0,0,0,4
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,4,1,1,1,1,1,1,1,0,0,4
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,1
  DEFB 1,1,1,1,1,8,0,0,2,0,0,2,0,0,2,4
  DEFB 0,0,0,0,0,0,0,5,1,1,1,1,1,1,8,0
  DEFB 0,2,0,0,2,0,0,0,0,0,0,0,0,0,0,4
  DEFB 5,1,1,1,1,1,1,8,0,0,0,2,0,0,2,0
  DEFB 0,0,0,0,0,0,0,0,5,1,1,1,1,1,1,4
  DEFB 4,0,2,0,0,2,0,0,2,0,0,0,0,0,0,0
  DEFB 0,5,1,1,1,1,1,1,1,1,1,1,1,1,1,4
  DEFB 4,0,0,0,0,0,0,0,0,0,5,1,1,1,1,1
  DEFB 1,8,0,0,0,2,0,0,2,0,0,2,0,0,2,4
  DEFB 4,0,0,5,1,1,1,1,1,1,4,0,0,0,2,0
  DEFB 0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,4
  DEFB 4,0,0,7,1,1,1,1,1,1,8,0,0,0,0,0
  DEFB 0,0,0,0,1,1,1,1,1,1,1,1,1,0,0,4
  DEFB 3,0,0,0,0,2,0,0,2,0,0,2,0,0,0,0
  DEFB 0,0,0,0,0,2,0,0,2,0,0,2,0,0,2,4
  DEFB 3,0,0,0,0,0,0,0,0,0,0,0,0,5,1,1
  DEFB 1,6,0,0,0,0,0,0,0,0,0,0,0,0,0,4
  DEFB 7,1,1,1,1,1,1,1,1,1,1,1,1,8,0,0
  DEFB 0,7,1,1,1,1,1,1,1,1,1,1,1,1,1,8

; Maze 4 layout
;
; Used by the routine at DRAWMAZE.
MAZE4:
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
  DEFB 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,6
  DEFB 3,0,0,4,9,9,4,0,0,0,0,0,0,0,2,0
  DEFB 0,2,0,0,0,0,0,0,0,4,9,9,4,0,0,4
  DEFB 3,0,0,4,0,0,4,0,0,0,0,2,0,0,0,0
  DEFB 0,0,0,0,2,0,0,0,0,4,0,0,4,0,0,4
  DEFB 4,0,0,4,0,0,4,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,4,0,0,4,0,0,4
  DEFB 4,0,0,4,0,0,4,0,0,2,0,0,0,0,2,0
  DEFB 0,2,0,0,0,0,0,2,0,4,0,0,4,0,0,4
  DEFB 4,0,0,4,0,0,4,0,0,0,0,5,6,0,0,0
  DEFB 0,0,0,5,6,0,0,0,0,4,0,0,4,0,0,4
  DEFB 4,0,0,4,0,0,4,0,0,0,0,7,8,0,0,0
  DEFB 0,0,0,7,8,0,0,0,0,4,0,0,4,0,0,4
  DEFB 4,0,0,4,0,0,4,0,2,0,0,2,0,0,0,0
  DEFB 0,0,0,0,2,0,0,0,2,4,0,0,0,0,0,4
  DEFB 4,0,0,4,0,0,4,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,4,0,0,0,0,0,4
  DEFB 4,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,7,1,1,6,0,0,4
  DEFB 4,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,4,0,0,4
  DEFB 4,0,0,7,1,1,6,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,4,0,0,4
  DEFB 4,0,0,0,0,0,4,0,0,0,0,2,0,0,0,0
  DEFB 0,0,0,0,2,0,0,0,0,4,0,0,4,0,0,4
  DEFB 4,0,0,0,0,0,4,0,2,0,0,5,6,0,0,0
  DEFB 0,0,0,5,6,0,0,0,2,4,0,0,4,0,0,4
  DEFB 7,1,1,6,0,0,4,0,0,0,0,7,8,0,0,0
  DEFB 0,0,0,7,8,0,0,0,0,4,0,0,4,0,0,4
  DEFB 0,0,0,4,0,0,4,0,0,0,0,0,0,0,2,0
  DEFB 0,2,0,0,0,0,0,0,0,4,0,0,4,0,0,4
  DEFB 0,0,0,4,0,0,4,0,0,2,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,2,0,4,0,0,4,0,0,4
  DEFB 0,0,0,4,0,0,4,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,4,0,0,4,0,0,4
  DEFB 0,0,0,4,0,0,4,0,0,0,0,2,0,0,0,0
  DEFB 0,0,0,0,2,0,0,0,0,4,0,0,4,0,0,3
  DEFB 0,0,0,4,9,9,4,0,0,0,0,0,0,0,2,0
  DEFB 0,2,0,0,0,0,0,0,0,4,9,9,4,0,0,3
  DEFB 0,0,0,7,1,1,1,1,1,1,1,1,1,1,1,1
  DEFB 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0

; Maze tiles
;
; Used by the routine at DRAWMAZE.
MAZETILES:
  DEFB 0,0,0,0,0,0,0,0
  DEFB 0,255,255,0,0,255,255,0
  DEFB 0,0,0,32,248,80,248,32
  DEFB 0,24,12,6,127,6,12,24
  DEFB 102,102,102,102,102,102,102,102
  DEFB 0,127,127,96,96,103,103,102
  DEFB 0,254,254,6,6,230,230,102
  DEFB 102,103,103,96,96,127,127,0
  DEFB 102,230,230,6,6,254,254,0
  DEFB 0,0,0,0,0,0,0,0

; PASSES   SCORE       BEST
;
; Used by the routine at START.
STATUS:
  DEFM "PASSES   SCORE       BEST"
  DEFB 255                ; End marker.

; Sprite movement timer
;
; Initialised by the routine at START, and used by the routine at MVSPRITES.
; Decremented on each pass through the main loop; when it reaches zero, Horace
; and the guards are moved.
MVTIMER:
  DEFB 0

; Game speed parameter (1-8)
;
; Initialised to 8 by the routine at START and decremented every time Horace
; returns to maze 1 via maze 4. Also used by the routines at MVSPRITES,
; MVGUARD, DRAWBELL, RINGBELL and INITGCT. The value here determines the speed
; at which the game runs.
SPEED:
  DEFB 0

; Current maze number
;
; Initialised to 255 by the routine at START just before a game starts. Holds
; 0, 1, 2 or 3 while a game is in progress.
MAZENO:
  DEFB 0

; Redundant temporary variable
;
; Used by the routine at MVGUARD.
TEMPVAR:
  DEFB 0

; Lunch-drop countdown timer
;
; Initialised by the routine at START, and used by the routine at DROPLUNCH.
; When this timer reaches zero, one of the guards will drop his lunch.
LUNCHCT:
  DEFW 0

; Address of the next pseudo-random number
;
; Initialised by the routine at START, and used by the routine at RANDOM.
RANDADDR:
  DEFW 0

; Sound on/off indicator
;
; Initialised by the routine at START, and used by the routines at MVSPRITES,
; CHKEATEN, CHKHIT, DEAD, TOGGLESND, RINGBELL and TITLESOUND. Holds 00011111
; binary if the sound is on, or 00000111 binary otherwise.
SOUND:
  DEFB 0

; 'T' pressed indicator
;
; Initialised by the routine at START, and used by the routine at TOGGLESND.
; Holds 0 if 'T' was being pressed the last time it was checked, or 16
; otherwise.
TPRESS:
  DEFB 0

; Horace's current location
;
; Initialised by the routine at START, and used by the routines at READKEYS,
; MVSPRITES, MVGUARD, REDRAW, DEAD, DRAWHORACE and DRAWBELL. Holds the display
; file address minus 16384.
HORACELOC:
  DEFW 0

; Horace's new location
;
; Initialised by the routine at START, and used by the routines at MVSPRITES,
; REDRAW, CHKEATEN, CHKHIT and DRAWHORACE. Holds the display file address minus
; 16384.
HORACENLOC:
  DEFW 0

; Temporary store for direction keypress
;
; Used by the routine at READKEYS.
DIRKEY:
  DEFB 0

; Horace's animation frame (0-3)
;
; Initialised by the routine at START, and used by the routines at READKEYS,
; MVSPRITES and DRAWHORACE.
HORACEAF:
  DEFB 0

; Horace's attribute byte
;
; Initialised by the routine at START, and used by the routines at DEAD and
; DRAWHORACE.
HORACEATTR:
  DEFB 0

; Horace's walking animation timer
;
; Used by the routine at DRAWHORACE. Incremented whenever Horace is drawn; bit
; 4 determines which set of animation frames (see HORACE0) is used: 0-3 if it's
; set, or 4-7 otherwise.
HORACETIME:
  DEFB 0

; Score
;
; Initialised by the routine at START, and used by the routines at CHKEATEN,
; CHKHIT, DEAD and DRAWBELL.
SCORE:
  DEFW 0

; High score
;
; Used by the routines at START and DEAD.
HISCORE:
  DEFW 0

; Extra life indicator
;
; Initialised by the routine at START, and used by the routine at CHKEATEN.
; Holds INT(S/10000)+2, where 'S' is the current score. When this value
; changes, Horace gets a new life.
ELIFEIND:
  DEFB 0

; Number of lives remaining
;
; Initialised by the routine at START, and used by the routines at CHKEATEN and
; DEAD.
LIVES:
  DEFB 0

; Tunnel timer
;
; Used by the routines at READKEYS, MVSPRITES, CHKEATEN, DEAD and DRAWHORACE.
; Holds a non-zero value when Horace is in a tunnel.
TUNNELTIME:
  DEFB 0

; Temporary store for Horace's screen x- and y-coordinates
;
; Used by the routine at MVGUARD.
TEMPHXY:
  DEFB 0                  ; x-coordinate.
  DEFB 0                  ; y-coordinate.

; Cherry and strawberry graphics
;
; Used by the routine at DROPLUNCH.
CHERRY:
  DEFB 60,0,0,1,1,2,2,2,1             ;
  DEFB 60,96,192,224,96,32,0,0,0      ;
  DEFB 58,6,14,15,15,7,3,0,0          ;
  DEFB 58,192,224,224,224,192,128,0,0 ;
STRAWBERRY:
  DEFB 60,0,0,0,0,16,28,14,3           ;
  DEFB 60,0,0,0,0,0,56,240,192         ;
  DEFB 58,30,23,29,31,13,7,3,0         ;
  DEFB 58,120,232,120,216,240,96,192,0 ;

; Bell location
;
; Initialised by the routine at START, and used by the routines at DRAWBELL and
; RINGBELL. Holds the location of the bell in the current maze, or zero if
; Horace has sounded the alarm.
BELLLOC:
  DEFW 0

; Bell animation frame counter
;
; Used by the routines at DRAWBELL and RINGBELL. Incremented on each pass
; through the main loop; its value determines which bell animation frame is
; used (see BELL), and also the bell sound effect.
BELLANIMFC:
  DEFB 0

; Horace graphics
;
; Used by the routine at DRAWHORACE.
HORACE0:
  DEFB 15,31,127,127,255,159,231,120   ;
  DEFB 224,248,254,254,255,241,143,126 ;
  DEFB 127,59,24,24,56,60,68,124       ;
  DEFB 216,216,28,36,124,0,0,0         ;
  DEFB 15,63,127,252,248,184,152,157  ;
  DEFB 248,252,254,115,115,99,230,238 ;
  DEFB 15,35,63,51,30,2,0,0           ;
  DEFB 252,188,158,142,12,28,18,63    ;
  DEFB 7,31,127,97,192,224,243,127     ;
  DEFB 240,248,254,198,195,195,227,254 ;
  DEFB 27,27,56,36,62,0,0,0            ;
  DEFB 254,220,24,24,28,52,34,62       ;
  DEFB 31,63,127,204,206,70,119,63    ;
  DEFB 224,248,254,62,31,31,31,185    ;
  DEFB 31,14,12,29,21,60,1,3          ;
  DEFB 249,240,224,192,192,224,48,224 ;
  DEFB 7,31,127,127,255,143,241,126   ;
  DEFB 240,248,254,254,255,249,231,30 ;
  DEFB 27,27,56,36,62,0,0,0           ;
  DEFB 254,220,24,24,28,60,34,62      ;
  DEFB 7,31,127,124,248,248,248,157  ;
  DEFB 248,252,254,51,115,98,238,252 ;
  DEFB 159,15,7,3,3,7,12,7           ;
  DEFB 248,112,48,184,168,60,128,192 ;
  DEFB 15,31,127,99,195,195,199,127 ;
  DEFB 224,248,254,134,3,7,207,254  ;
  DEFB 127,59,24,24,56,44,68,124    ;
  DEFB 216,216,28,36,124,0,0,0      ;
  DEFB 31,63,127,206,206,198,103,119 ;
  DEFB 240,252,254,63,31,29,25,185   ;
  DEFB 63,61,121,113,48,56,72,252    ;
  DEFB 240,196,252,204,120,64,0,0    ;

; Guard graphics
;
; Used by the routine at DRAWGUARD.
GUARD0:
  DEFB 31,63,63,63,79,113,126,127      ;
  DEFB 248,252,252,252,250,198,62,254  ;
  DEFB 95,95,63,63,31,15,7,3           ;
  DEFB 250,250,252,252,248,240,224,192 ;
  DEFB 31,63,63,63,63,60,124,76       ;
  DEFB 224,240,240,248,255,68,68,238  ;
  DEFB 92,94,111,63,63,31,31,7        ;
  DEFB 238,34,252,248,184,128,248,192 ;
  DEFB 31,63,63,63,127,99,65,102       ;
  DEFB 248,252,252,252,254,142,134,154 ;
  DEFB 118,121,63,63,31,12,7,3         ;
  DEFB 218,230,252,252,248,48,224,192  ;
  DEFB 7,15,15,31,255,34,34,93         ;
  DEFB 248,252,252,252,252,60,62,242   ;
  DEFB 93,68,63,31,31,17,31,3          ;
  DEFB 250,122,246,252,252,248,248,224 ;
  DEFB 31,63,63,63,95,99,124,127       ;
  DEFB 248,252,252,252,242,142,126,254 ;
  DEFB 95,95,63,63,31,15,7,3           ;
  DEFB 250,250,252,252,248,240,224,192 ;
  DEFB 31,63,63,63,63,60,124,79       ;
  DEFB 224,240,240,248,255,68,68,186  ;
  DEFB 95,94,111,63,63,31,31,7        ;
  DEFB 186,34,252,248,248,136,248,192 ;
  DEFB 31,63,63,63,127,113,97,89       ;
  DEFB 248,252,252,252,254,198,130,102 ;
  DEFB 91,103,63,63,24,12,6,3          ;
  DEFB 110,158,252,252,24,48,96,192    ;
  DEFB 7,15,15,31,255,34,34,119       ;
  DEFB 248,252,252,252,252,60,62,50   ;
  DEFB 119,68,63,31,29,1,31,3         ;
  DEFB 58,122,246,252,252,248,248,224 ;
  DEFB 136,200,76,102,55,164,160,230 ;
  DEFB 64,96,96,198,204,60,9,199     ;
  DEFB 70,96,33,35,22,12,6,3         ;
  DEFB 194,6,132,204,104,48,96,192   ;
  DEFB 196,68,102,38,55,184,160,227 ;
  DEFB 16,48,96,231,206,60,8,103    ;
  DEFB 67,96,33,35,22,12,6,3        ;
  DEFB 98,6,132,204,104,48,96,192   ;

; Bell graphics
;
; Used by the routine at DRAWBELL.
BELL:
  DEFB 0,0,0,2,1,1,1,1         ;
  DEFB 0,0,0,0,224,249,254,252 ;
  DEFB 0,0,0,1,1,0,0,0         ;
  DEFB 240,224,224,160,0,0,0,0 ;
  DEFB 0,0,0,1,1,1,3,3       ;
  DEFB 0,0,0,0,0,192,224,224 ;
  DEFB 3,7,7,29,1,0,0,0      ;
  DEFB 240,252,224,0,0,0,0,0 ;
  DEFB 0,0,0,0,0,3,7,7           ;
  DEFB 0,0,0,128,128,128,192,192 ;
  DEFB 15,63,7,0,0,0,0,0         ;
  DEFB 192,224,224,184,128,0,0,0 ;
  DEFB 0,0,0,0,7,159,127,63     ;
  DEFB 0,0,0,64,128,128,128,128 ;
  DEFB 15,7,7,5,0,0,0,0         ;
  DEFB 0,0,0,128,128,0,0,0      ;

; Blank sprite
;
; Used by the routines at REDRAW, DEAD and DRAWBELL.
BLANK:
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;

